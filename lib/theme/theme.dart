import 'package:flutter/material.dart';

final appTheme = ThemeData(
  brightness: Brightness.light,
  // primaryColor: Color(264),
  appBarTheme: AppBarTheme(
    color: Color.fromRGBO(45, 54, 147, 1),
  ),
  // primarySwatch: MaterialColor(),
  fontFamily: 'Almarai',
  scaffoldBackgroundColor: Colors.white,
);
