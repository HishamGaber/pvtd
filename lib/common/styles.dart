import 'package:flutter/material.dart';

const Color white = Colors.white;
const Color black = Colors.black;

const TextStyle TITLE_STYLE = TextStyle(
  color: const Color(0xff26262b),
  fontWeight: FontWeight.w700,
  fontFamily: "Almarai",
  fontStyle: FontStyle.normal,
  fontSize: 25.0,
  height: 1.5,
);

const TextStyle SUBTITLE = TextStyle(
  color: const Color(0xff26262b),
  fontWeight: FontWeight.w700,
  fontFamily: "Almarai",
  fontStyle: FontStyle.normal,
  fontSize: 16.0,
  height: 1.5,
);

const TextStyle GREENSTYLE = TextStyle(
  color: const Color.fromRGBO(62, 177, 91, 1),
  fontWeight: FontWeight.w700,
  fontFamily: "Almarai",
  fontStyle: FontStyle.normal,
  fontSize: 18.0,
);
const TextStyle REDSTYLE = TextStyle(
  color: const Color.fromRGBO(209, 10, 5, 1),
  fontWeight: FontWeight.w700,
  fontFamily: "Almarai",
  fontStyle: FontStyle.normal,
  fontSize: 18.0,
);

const TextStyle GREYSTYLE = TextStyle(
  color: const Color.fromRGBO(183, 183, 184, 1),
  fontWeight: FontWeight.w700,
  fontFamily: "Almarai",
  fontStyle: FontStyle.normal,
  fontSize: 18.0,
);

// const ButtonStyle defaultElevatedButtonStyle = ButtonStyle(
//   backgroundColor: MaterialStateProperty.all(
//     Color.fromRGBO(45, 54, 147, 1),
//   ),
// );
