import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/providers/myRequestsResponse.dart';
import 'package:pvtd_app/providers/userById.dart';
import 'package:pvtd_app/screens/companyEdit.dart';
import 'package:pvtd_app/screens/individualEdit.dart';
import 'package:pvtd_app/screens/myrequests_screen.dart';
import 'package:pvtd_app/screens/services/trainingApply.dart';
import 'package:pvtd_app/screens/signup.dart';
import 'package:pvtd_app/screens/signupIndividual.dart';
import 'package:pvtd_app/screens/signupStudent.dart';
import 'package:pvtd_app/screens/studentEdit.dart';
import 'package:pvtd_app/screens/updatepassword.dart';
import '/providers/institutions.dart' as inst;
import '/providers/news.dart';
import '/providers/trackType.dart';
import '/providers/tracks.dart' as tr;
import '/screens/news_screen.dart';
import '/screens/services/certificate_service.dart';
import '/screens/services/maintenance_service.dart';
import '/screens/services/manifacturing_service.dart';
import '/screens/services/skill_service.dart';
import '/screens/services/station_service.dart';
import '/screens/services/training_service.dart';
import '/screens/services_screen.dart';
import '/screens/signin.dart';
import '/screens/done.dart';
import '/screens/fail.dart';
import '/screens/home_screen.dart';
import '/screens/services/consolation_service.dart';
import '/theme/theme.dart';

import '/providers/governorate.dart' as gov;
import 'providers/auth.dart';
import 'providers/services.dart';
import 'screens/signupCompany.dart';
import 'screens/splash_screen.dart';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<Auth>(
          create: (_) => Auth(),
        ),
        ChangeNotifierProvider<Service>(
          create: (_) => Service(),
        ),
        ChangeNotifierProvider<inst.Institution>(
          create: (_) => inst.Institution(),
        ),
        ChangeNotifierProvider<tr.InstitutionTraining>(
          create: (_) => tr.InstitutionTraining(),
        ),
        ChangeNotifierProvider<TrackType>(
          create: (_) => TrackType(),
        ),
        ChangeNotifierProvider<News>(
          create: (_) => News(),
        ),
        ChangeNotifierProvider<MyRequestsResponse>(
          create: (_) => MyRequestsResponse(),
        ),
        ChangeNotifierProvider<UserById>(
          create: (_) => UserById(),
        ),
        ChangeNotifierProvider<gov.Governorate>(
          create: (_) => gov.Governorate(),
        ),
      ],
      child: MaterialApp(
        builder: (context, child) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: child,
          );
        },
        debugShowCheckedModeBanner: false,
        title: 'Provider Example',
        theme: appTheme,
        home: Splash(),
        routes: {
          HomeScreen.routeName: (context) => HomeScreen(),
          ConsolationService.routeName: (context) => ConsolationService(),
          MaintenanceService.routeName: (context) => MaintenanceService(),
          CertificateService.routeName: (context) => CertificateService(),
          ManufacturingService.routeName: (context) => ManufacturingService(),
          NewsScreen.routeName: (context) => NewsScreen(),
          ServicesScreen.routeName: (context) => ServicesScreen(),
          TrainingService.routeName: (context) => TrainingService(),
          StationService.routeName: (context) => StationService(),
          SkillService.routeName: (context) => SkillService(),
          Fail.routeName: (context) => Fail(),
          Done.routeName: (context) => Done(),
          Signin.routeName: (context) => Signin(),
          MyRequestsScreen.routeName: (context) => MyRequestsScreen(),
          IndividualEdit.routeName: (context) => IndividualEdit(),
          Signup.routeName: (context) => Signup(),
          SignupIndividual.routeName: (context) => SignupIndividual(),
          SignupStudent.routeName: (context) => SignupStudent(),
          SignupCompany.routeName: (context) => SignupCompany(),
          UpdatePassword.routeName: (context) => UpdatePassword(),
          CompanyEdit.routeName: (context) => CompanyEdit(),
          StudentEdit.routeName: (context) => StudentEdit(),
          TrainingApply.routeName: (context) => TrainingApply(),
        },
        navigatorKey: key,
      ),
    );
  }
}
