import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/governorate.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/tracks.dart';

class DefaultInstitutionDropDown extends StatelessWidget {
  @required
  final Function onChanged;
  @required
  final String label;
  DefaultInstitutionDropDown({this.onChanged, this.label}) : super();

  @override
  Widget build(BuildContext context) {
    return Consumer<Institution>(builder: (context, model, child) {
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                label,
                style: SUBTITLE,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          DropdownSearch<Institution>(
            validator: (value) {
              if (value == null) {
                return 'من فضلك ادخل المركز';
              }
              return null;
            },
            dropdownBuilderSupportsNullItem: true,
            showSearchBox: true,
            label: '   المركز',
            items: model.institutions,
            itemAsString: (Institution u) => '   ' + u.arTitle,
            onChanged: onChanged,
          ),
        ],
      );
    });
  }
}

class DefaultTracksDropDown extends StatelessWidget {
  @required
  final String label;
  @required
  final Function onChanged;
  DefaultTracksDropDown({this.label, this.onChanged}) : super();

  @override
  Widget build(BuildContext context) {
    return Consumer<InstitutionTraining>(builder: (context, model, child) {
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                label,
                style: SUBTITLE,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          DropdownSearch<InstitutionTraining>(
            validator: (value) {
              if (value == null) {
                return 'من فضلك ادخل المهارة';
              }
              return null;
            },
            dropdownBuilderSupportsNullItem: true,
            showSearchBox: true,
            label: '   المهارات',
            items: model.tracksOfInstitution,
            itemAsString: (InstitutionTraining u) =>
                '   ' + u.eductionTrack.arTitle,
            onChanged: onChanged,
          ),
        ],
      );
    });
  }
}

class DefaultEntityDropDown extends StatelessWidget {
  @required
  final String label;
  @required
  final Function onChanged;
  DefaultEntityDropDown({this.label, this.onChanged}) : super();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              label,
              style: SUBTITLE,
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        DropdownSearch<String>(
          validator: (value) {
            if (value == null) {
              return 'من فضلك ادخل الجهة';
            }
            return null;
          },
          dropdownBuilderSupportsNullItem: true,
          showSearchBox: true,
          label: '   الجهة',
          items: ['شركة', 'فرد'],
        ),
      ],
    );
  }
}

class DefaultGovernorateDropDown extends StatelessWidget {
  @required
  final Function onChanged;
  DefaultGovernorateDropDown({this.onChanged}) : super();

  @override
  Widget build(BuildContext context) {
    return Consumer<Governorate>(
      builder: (BuildContext context, model, Widget child) {
        return Expanded(
          child: DropdownSearch<Governorate>(
            validator: (value) {
              if (value == null) {
                return 'من فضلك ادخل المحافظة';
              }
              return null;
            },
            items: model.governorates,
            // value: 'dropdownValueInstitutions',
            hint: "   اختر المحافظة",
            onChanged: onChanged,
            itemAsString: (Governorate u) => '   ' + u.arTitle,
            showSearchBox: true,
          ),
        );
      },
    );
  }
}

class DefaultLanguageDropDown extends StatelessWidget {
  @required
  final String label;
  @required
  final Function onChanged;
  DefaultLanguageDropDown({this.label, this.onChanged}) : super();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              label,
              style: SUBTITLE,
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        DropdownSearch<String>(
          validator: (value) {
            if (value == null) {
              return 'من فضلك ادخل الجهة';
            }
            return null;
          },
          dropdownBuilderSupportsNullItem: true,
          showSearchBox: true,
          label: '   اللغة الأجنبية التي تعلمها التلميذ',
          items: ['FRENCH', 'ENGLISH', 'GERMAN'],
        ),
      ],
    );
  }
}

class YearWidget extends StatelessWidget {
  final String text;
  final String hinttext;
  final TextEditingController controller;

  const YearWidget({
    @required this.text,
    @required this.hinttext,
    @required this.controller,
  }) : super();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(text,
                style: const TextStyle(
                  color: const Color(0xff26262b),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 16.0,
                ),
                textAlign: TextAlign.right),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'من فضلك ادخل السنة';
            }

            return null;
          },
          keyboardType: TextInputType.number,
          controller: controller,
          textAlign: TextAlign.right,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
