import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pvtd_app/providers/governorate.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/trackType.dart';
import 'package:pvtd_app/providers/tracks.dart';
import '../common/api_urls.dart';

class FetchApi {
  static Future<GovernorateResponse> fetchAndSetGovernorates(
      [String searchKey]) async {
    final response =
        await http.get(Uri.parse(MAIN_URL + GOVERNORATES + '?limit=50'));

    GovernorateResponse governorateResponse =
        GovernorateResponse.fromJson(json.decode(response.body));
    print('object');
    return governorateResponse;
  }

  static Future<InstitutionsResponse> fetchAndSetInstitutions(
      [String searchKey]) async {
    final response =
        await http.get(Uri.parse(MAIN_URL + INSTITUTIONS_URL + '?limit=50'));

    InstitutionsResponse institutionsResponse =
        InstitutionsResponse.fromJson(json.decode(response.body));
    print('object');
    return institutionsResponse;
  }

  static Future<InstitutionTracksResponse> fetchAndSetTracks() async {
    final response =
        await http.get(Uri.parse(MAIN_URL + TRACKS_URL + '?limit=50'));

    InstitutionTracksResponse institutionsTracksResponse =
        InstitutionTracksResponse.fromJson(json.decode(response.body));

    return institutionsTracksResponse;
  }

  static Future<InstitutionTracksResponse> fetchAndSetTracksByInstitution(
      String institutionId) async {
    final response = await http
        .get(Uri.parse(MAIN_URL + TRACKS_URL_INSTITUTION + institutionId));

    InstitutionTracksResponse institutionsTracksResponse =
        InstitutionTracksResponse.fromJson(json.decode(response.body));

    return institutionsTracksResponse;
  }

  static Future<InstitutionTracksResponse> fetchAndSetPrograms(
      String trackTypeId) async {
    final response =
        await http.get(Uri.parse(MAIN_URL + PROGRAM_URL + trackTypeId));

    InstitutionTracksResponse institutionsTracksResponse =
        InstitutionTracksResponse.fromJson(json.decode(response.body));

    return institutionsTracksResponse;
  }

  static Future<TrackTypeResponse> fetchAndSetTrackTypes(
      [String searchKey]) async {
    final response =
        await http.get(Uri.parse(MAIN_URL + TRACK_TYPES + '?limit=50'));

    TrackTypeResponse trackTypeResponse =
        TrackTypeResponse.fromJson(json.decode(response.body));
    print('object');

    return trackTypeResponse;

    // notifyListeners();
  }
}
