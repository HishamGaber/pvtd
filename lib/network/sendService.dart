import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:pvtd_app/common/api_urls.dart';
import 'package:pvtd_app/providers/common_response.dart';

class SendService {
  static Future<CommonResponse> sendService({
    Map<String, dynamic> body,
    String token,
    String url,
  }) async {
    http.Response response = await http.post(
      Uri.parse(MAIN_URL + url),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        'Content-Type': 'application/json',
      },
      body: json.encode(body),
    );
    try {
      print(response.body);
      final decodeBody = json.decode(response.body);
      return CommonResponse.fromMap(decodeBody);
    } catch (error) {
      return CommonResponse.onError();
    }
  }
}
