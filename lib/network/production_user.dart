import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '/providers/user.dart';

import '../common/api_urls.dart';

class ProductionUser {
  static Future<AuthResponse> login(
    String email,
    String password,
  ) {
    print('$email ff $password');
    return http
        .post(
      Uri.parse(MAIN_URL + LOGIN_URL),
      headers: {
        "Content-Type": "application/json",
      },
      body: json.encode({
        'email': email,
        'password': password,
      }),
    )
        .then(
      (response) {
        final body = response.body;
        try {
          final decodeBody = json.decode(body);
          print(':::::: $decodeBody');

          return AuthResponse.fromMap(decodeBody);
        } catch (ex) {
          print('error');
          print(ex);
          return AuthResponse.onError();
        }
      },
    );
  }

  static Future<AuthResponse> signupStudent(
    String username,
    String email,
    String mobile,
    String nationalid,
    String password,
  ) {
    return http
        .post(
      Uri.parse(MAIN_URL + SIGNUPSTUDENT_URL),
      headers: {
        "Content-Type": "application/json",
      },
      body: json.encode({
        "phone": mobile,
        "email": email,
        "password": password,
        "username": username,
        "nationalId": nationalid,
      }),
    )
        .then(
      (response) {
        final body = response.body;
        try {
          final decodeBody = json.decode(body);
          print(':::::: $decodeBody');
          return AuthResponse.fromMap(decodeBody);
        } catch (ex) {
          print('error');
          print(ex);
          return AuthResponse.onError();
        }
      },
    );
  }

  static Future<AuthResponse> signupCompany(
    String companyName,
    String username,
    String email,
    String password,
  ) {
    return http
        .post(
      Uri.parse(MAIN_URL + SIGNUPCOMPANY_URL),
      headers: {
        "Content-Type": "application/json",
      },
      body: json.encode({
        "companyName": companyName,
        "email": email,
        "password": password,
        "username": username,
      }),
    )
        .then(
      (response) {
        final body = response.body;
        try {
          final decodeBody = json.decode(body);
          print(':::::: $decodeBody');
          return AuthResponse.fromMap(decodeBody);
        } catch (ex) {
          print('error');
          print(ex);
          return AuthResponse.onError();
        }
      },
    );
  }

  static Future<AuthResponse> signupIndividual(
    String email,
    String password,
    String username,
  ) {
    return http
        .post(
      Uri.parse(MAIN_URL + SIGNUPINDIVIDUAL_URL),
      headers: {
        "Content-Type": "application/json",
      },
      body: json.encode({
        "email": email,
        "password": password,
        "username": username,
      }),
    )
        .then(
      (response) {
        final body = response.body;
        try {
          final decodeBody = json.decode(body);
          print(':::::: $decodeBody');
          return AuthResponse.fromMap(decodeBody);
        } catch (ex) {
          print('error');
          print(ex);
          return AuthResponse.onError();
        }
      },
    );
  }
}
