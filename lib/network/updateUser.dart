import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:pvtd_app/common/api_urls.dart';
import 'package:pvtd_app/providers/common_response.dart';
import 'package:pvtd_app/providers/requestResponse.dart';

class UpdateUser {
  static Future<CommonResponse> updateUser({
    Map<String, dynamic> body,
    String token,
    String url,
  }) async {
    http.Response response = await http.put(
      Uri.parse(MAIN_URL + url),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        'Content-Type': 'application/json',
      },
      body: json.encode(body),
    );
    try {
      final decodeBody = json.decode(response.body);
      return CommonResponse.fromMap(decodeBody);
    } catch (error) {
      return CommonResponse.onError();
    }
  }

  static Future<RequestResponse> updatePassword({
    Map<String, dynamic> body,
    String token,
    String url,
  }) async {
    http.Response response = await http.put(
      Uri.parse(MAIN_URL + url),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        'Content-Type': 'application/json',
      },
      body: json.encode(body),
    );
    try {
      final decodeBody = json.decode(response.body);
      return RequestResponse.fromMap(decodeBody);
    } catch (error) {
      return RequestResponse.onError();
    }
  }
}
