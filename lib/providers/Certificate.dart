import 'package:flutter/material.dart';

class Certificate {
  Certificate({
    @required this.institutionId,
    @required this.eductionTrackId,
    @required this.targetPlace,
    @required this.graduationYear,
    @required this.transcripts,
    @required this.originalGraduationCertificate,
    @required this.certificateGraduationExtract,
    @required this.successStatement,
    @required this.statementCase,
    @required this.skill,
  });

  String institutionId;
  String eductionTrackId;
  String targetPlace;
  String graduationYear;
  bool transcripts;
  bool originalGraduationCertificate;
  bool certificateGraduationExtract;
  bool successStatement;
  bool statementCase;
  bool skill;

  factory Certificate.fromJson(Map<String, dynamic> json) => Certificate(
        institutionId: json["InstitutionId"],
        eductionTrackId: json["EductionTrackId"],
        targetPlace: json["targetPlace"],
        graduationYear: json["graduationYear"],
        transcripts: json["transcripts"],
        originalGraduationCertificate: json["originalGraduationCertificate"],
        certificateGraduationExtract: json["certificateGraduationExtract"],
        successStatement: json["successStatement"],
        statementCase: json["statementCase"],
        skill: json["skill"],
      );

  Map<String, dynamic> toJson() => {
        "InstitutionId": institutionId,
        "EductionTrackId": eductionTrackId,
        "targetPlace": targetPlace,
        "graduationYear": graduationYear,
        "transcripts": transcripts,
        "originalGraduationCertificate": originalGraduationCertificate,
        "certificateGraduationExtract": certificateGraduationExtract,
        "successStatement": successStatement,
        "statementCase": statementCase,
        "skill": skill,
      };
}
