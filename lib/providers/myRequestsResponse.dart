// To parse this JSON data, do
//
//     final myRequestsResponse = myRequestsResponseFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '/common/api_urls.dart';

// MyRequestsResponse myRequestsResponseFromJson(String str) =>
//     MyRequestsResponse.fromJson(json.decode(str));

// String myRequestsResponseToJson(MyRequestsResponse data) =>
//     json.encode(data.toJson());

class MyRequestsResponse with ChangeNotifier {
  MyRequestsResponse({
    this.user,
  });

  UserRequests user;
  MyRequestsResponse myRequestsResponse;

  factory MyRequestsResponse.fromJson(Map<String, dynamic> json) =>
      MyRequestsResponse(
        user: UserRequests.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
      };
  Future<void> fetchAndSetMyRequests(String token) async {
    try {
      final response = await http.get(
        Uri.parse(MAIN_URL + SERVICE_REQUESTS),
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          'Content-Type': 'application/json',
        },
      );
      print(json.decode(response.body));
      myRequestsResponse =
          MyRequestsResponse.fromJson(json.decode(response.body));
    } catch (e) {
      print('hisham ' + e.toString());
    }
  }
}

class UserRequests with ChangeNotifier {
  UserRequests({
    this.id,
    this.firstName,
    this.lastName,
    this.avatar,
    this.phone,
    this.nationalId,
    this.email,
    this.username,
    this.name,
    this.role,
    this.password,
    this.active,
    this.address,
    this.landLine,
    this.restToken,
    this.restTokenExpireAt,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.governorateId,
    this.institutionId,
    this.requestTrainings,
    this.requestSetupStations,
    this.requestMeasurementSkills,
    this.requestManufacturings,
    this.requestMaintenances,
    this.requestConsolations,
    this.requestCertificates,
    this.requestApplyingToTrainings,
  });

  int id;
  dynamic firstName;
  dynamic lastName;
  dynamic avatar;
  String phone;
  String nationalId;
  String email;
  dynamic username;
  String name;
  String role;
  String password;
  bool active;
  String address;
  dynamic landLine;
  dynamic restToken;
  dynamic restTokenExpireAt;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;
  String governorateId;
  dynamic institutionId;
  List<RequestTrainings> requestTrainings;
  List<RequestSetupStations> requestSetupStations;
  List<RequestMeasurementSkills> requestMeasurementSkills;
  List<RequestManufacturings> requestManufacturings;
  List<RequestMaintenances> requestMaintenances;
  List<RequestConsolation> requestConsolations;
  List<RequestCertificate> requestCertificates;
  List<RequestApplyingToTrainings> requestApplyingToTrainings;

  factory UserRequests.fromJson(Map<String, dynamic> json) => UserRequests(
        id: json["id"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        avatar: json["avatar"],
        phone: json["phone"],
        nationalId: json["nationalId"],
        email: json["email"],
        username: json["username"],
        name: json["name"],
        role: json["role"],
        password: json["password"],
        active: json["active"],
        address: json["address"],
        landLine: json["landLine"],
        restToken: json["restToken"],
        restTokenExpireAt: json["restTokenExpireAt"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        governorateId: json["GovernorateId"],
        institutionId: json["InstitutionId"],
        requestTrainings: List<RequestTrainings>.from(
            json["RequestTrainings"].map((x) => RequestTrainings.fromJson(x))),
        requestSetupStations: List<RequestSetupStations>.from(
            json["RequestSetupStations"]
                .map((x) => RequestSetupStations.fromJson(x))),
        requestMeasurementSkills: List<RequestMeasurementSkills>.from(
            json["RequestMeasurementSkills"]
                .map((x) => RequestMeasurementSkills.fromJson(x))),
        requestManufacturings: List<RequestManufacturings>.from(
            json["RequestManufacturings"]
                .map((x) => RequestManufacturings.fromJson(x))),
        requestMaintenances: List<RequestMaintenances>.from(
            json["RequestMaintenances"]
                .map((x) => RequestMaintenances.fromJson(x))),
        requestConsolations: List<RequestConsolation>.from(
            json["RequestConsolations"]
                .map((x) => RequestConsolation.fromJson(x))),
        requestCertificates: List<RequestCertificate>.from(
            json["RequestCertificates"]
                .map((x) => RequestCertificate.fromJson(x))),
        requestApplyingToTrainings: List<RequestApplyingToTrainings>.from(
            json["RequestApplyingToTrainings"]
                .map((x) => RequestApplyingToTrainings.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "avatar": avatar,
        "phone": phone,
        "nationalId": nationalId,
        "email": email,
        "username": username,
        "name": name,
        "role": role,
        "password": password,
        "active": active,
        "address": address,
        "landLine": landLine,
        "restToken": restToken,
        "restTokenExpireAt": restTokenExpireAt,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "deletedAt": deletedAt,
        "GovernorateId": governorateId,
        "InstitutionId": institutionId,
        "RequestTrainings": List<dynamic>.from(requestTrainings.map((x) => x)),
        "RequestSetupStations":
            List<dynamic>.from(requestSetupStations.map((x) => x)),
        "RequestMeasurementSkills":
            List<dynamic>.from(requestMeasurementSkills.map((x) => x)),
        "RequestManufacturings":
            List<dynamic>.from(requestManufacturings.map((x) => x)),
        "RequestMaintenances":
            List<dynamic>.from(requestMaintenances.map((x) => x)),
        "RequestConsolations":
            List<dynamic>.from(requestConsolations.map((x) => x)),
        "RequestCertificates":
            List<dynamic>.from(requestCertificates.map((x) => x.toJson())),
        "RequestApplyingToTrainings":
            List<dynamic>.from(requestApplyingToTrainings.map((x) => x)),
      };
}

class RequestCertificate {
  RequestCertificate({
    this.id,
    this.targetPlace,
    this.graduationYear,
    this.transcripts,
    this.originalGraduationCertificate,
    this.certificateGraduationExtract,
    this.successStatement,
    this.statementCase,
    this.skill,
    this.rejectionReasone,
    this.state,
    this.reciveDate,
    this.location,
    this.otherData,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.institutionId,
    this.userId,
    this.eductionTrackId,
  });

  String id;
  String targetPlace;
  String graduationYear;
  bool transcripts;
  bool originalGraduationCertificate;
  bool certificateGraduationExtract;
  bool successStatement;
  bool statementCase;
  bool skill;
  dynamic rejectionReasone;
  String state;
  dynamic reciveDate;
  dynamic location;
  dynamic otherData;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;
  String institutionId;
  int userId;
  String eductionTrackId;

  factory RequestCertificate.fromJson(Map<String, dynamic> json) =>
      RequestCertificate(
        id: json["id"],
        targetPlace: json["targetPlace"],
        graduationYear: json["graduationYear"],
        transcripts: json["transcripts"],
        originalGraduationCertificate: json["originalGraduationCertificate"],
        certificateGraduationExtract: json["certificateGraduationExtract"],
        successStatement: json["successStatement"],
        statementCase: json["statementCase"],
        skill: json["skill"],
        rejectionReasone: json["rejectionReasone"],
        state: json["state"],
        reciveDate: json["reciveDate"],
        location: json["location"],
        otherData: json["otherData"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        institutionId: json["InstitutionId"],
        userId: json["UserId"],
        eductionTrackId: json["EductionTrackId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "targetPlace": targetPlace,
        "graduationYear": graduationYear,
        "transcripts": transcripts,
        "originalGraduationCertificate": originalGraduationCertificate,
        "certificateGraduationExtract": certificateGraduationExtract,
        "successStatement": successStatement,
        "statementCase": statementCase,
        "skill": skill,
        "rejectionReasone": rejectionReasone,
        "state": state,
        "reciveDate": reciveDate,
        "location": location,
        "otherData": otherData,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "deletedAt": deletedAt,
        "InstitutionId": institutionId,
        "UserId": userId,
        "EductionTrackId": eductionTrackId,
      };
}

class RequestConsolation {
  RequestConsolation({
    this.state,
  });
  String state;

  factory RequestConsolation.fromJson(Map<String, dynamic> json) =>
      RequestConsolation(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}

class RequestApplyingToTrainings {
  RequestApplyingToTrainings({
    this.state,
  });
  String state;

  factory RequestApplyingToTrainings.fromJson(Map<String, dynamic> json) =>
      RequestApplyingToTrainings(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}

class RequestMaintenances {
  RequestMaintenances({
    this.state,
  });
  String state;

  factory RequestMaintenances.fromJson(Map<String, dynamic> json) =>
      RequestMaintenances(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}

class RequestManufacturings {
  RequestManufacturings({
    this.state,
  });
  String state;

  factory RequestManufacturings.fromJson(Map<String, dynamic> json) =>
      RequestManufacturings(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}

class RequestMeasurementSkills {
  RequestMeasurementSkills({
    this.state,
  });
  String state;

  factory RequestMeasurementSkills.fromJson(Map<String, dynamic> json) =>
      RequestMeasurementSkills(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}

class RequestSetupStations {
  RequestSetupStations({
    this.state,
  });
  String state;

  factory RequestSetupStations.fromJson(Map<String, dynamic> json) =>
      RequestSetupStations(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}

class RequestTrainings {
  RequestTrainings({
    this.state,
  });
  String state;

  factory RequestTrainings.fromJson(Map<String, dynamic> json) =>
      RequestTrainings(
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
      };
}
