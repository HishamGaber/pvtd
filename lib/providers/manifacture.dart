import 'package:flutter/material.dart';

class Manifacture {
  Manifacture({
    @required this.entity,
    @required this.entityName,
    @required this.description,
    @required this.paymentMethod,
    @required this.institutionId,
    @required this.providingRawMaterials,
    @required this.providingDimensionalDrawings,
  });

  String entity;
  String entityName;
  String description;
  String paymentMethod;
  String institutionId;
  bool providingRawMaterials;
  bool providingDimensionalDrawings;

  factory Manifacture.fromJson(Map<String, dynamic> json) => Manifacture(
        entity: json["entity"],
        entityName: json["entityName"],
        description: json["description"],
        paymentMethod: json["paymentMethod"],
        institutionId: json["InstitutionId"],
        providingRawMaterials: json["providingRawMaterials"],
        providingDimensionalDrawings: json["providingDimensionalDrawings"],
      );

  Map<String, dynamic> toJson() => {
        "entity": entity,
        "entityName": entityName,
        "description": description,
        "paymentMethod": paymentMethod,
        "InstitutionId": institutionId,
        "providingRawMaterials": providingRawMaterials,
        "providingDimensionalDrawings": providingDimensionalDrawings,
      };
}
