import 'package:pvtd_app/providers/common_response.dart';
import 'package:pvtd_app/providers/company.dart';
import 'package:pvtd_app/providers/requestResponse.dart';
import 'package:pvtd_app/providers/student.dart';
import 'package:pvtd_app/providers/trainingApplication.dart';

import '/network/updateUser.dart';
import '/providers/manifacture.dart';
import '/providers/station.dart';

import '/network/sendService.dart';
import '/providers/consolation.dart';
import '/providers/maintenance.dart';
import '/providers/skill.dart';

import '../common/api_urls.dart';
import 'Certificate.dart';
import 'individual.dart';
import 'password.dart';
import 'training.dart';

class SendRequest {
  static Future<CommonResponse> sendConsolation({
    Consolation consolation,
    String token,
  }) async {
    return await SendService.sendService(
      token: token,
      url: CONSOLATION_URL,
      body: consolation.toJson(),
    );
  }

  static Future<CommonResponse> sendMaintenance({
    Maintenance maintenance,
    String token,
  }) async {
    return await SendService.sendService(
      token: token,
      url: MAINTENANCE_URL,
      body: maintenance.toJson(),
    );
  }

  static Future<CommonResponse> sendCertificate({
    Certificate certificate,
    String token,
  }) async {
    return await SendService.sendService(
      token: token,
      url: CERTIFICATES_URL,
      body: certificate.toJson(),
    );
  }

  static Future<CommonResponse> sendSkill({
    Skill skill,
    String token,
  }) async {
    return await SendService.sendService(
      token: token,
      url: SKILL_URL,
      body: skill.toJson(),
    );
  }

  static Future<CommonResponse> sendStation({
    Station station,
    String token,
  }) async {
    final response = await SendService.sendService(
      token: token,
      url: STATION_URL,
      body: station.toJson(),
    );
    print(response);
    return response;
  }

  static Future<CommonResponse> sendManifacturing({
    Manifacture manifacture,
    String token,
  }) async {
    return await SendService.sendService(
      token: token,
      url: MANIFACTURING_URL,
      body: manifacture.toJson(),
    );
  }

  static Future<CommonResponse> sendTrainingApplication({
    TrainingApplication trainingApplication,
    String token,
  }) async {
    CommonResponse response;
    response = await SendService.sendService(
      token: token,
      url: TRAINING_APPLY_URL,
      body: trainingApplication.toJson(),
    );
    return response;
  }

  static Future<CommonResponse> sendTraining({
    Training training,
    String token,
  }) async {
    return await SendService.sendService(
      token: token,
      url: CONSOLATION_URL,
      body: training.toJson(),
    );
  }

  static Future<CommonResponse> updateIndividual({
    Individual indidvidual,
    String token,
  }) async {
    CommonResponse response;
    response = await UpdateUser.updateUser(
      token: token,
      url: UPDATE,
      body: indidvidual.toJson(),
    );
    return response;
  }

  static Future<CommonResponse> updateCompany({
    Company company,
    String token,
  }) async {
    CommonResponse response;
    response = await UpdateUser.updateUser(
      token: token,
      url: UPDATE,
      body: company.toJson(),
    );
    return response;
  }

  static Future<CommonResponse> updateStudent({
    Student student,
    String token,
  }) async {
    CommonResponse response;
    response = await UpdateUser.updateUser(
      token: token,
      url: UPDATE,
      body: student.toJson(),
    );
    return response;
  }

  static Future<RequestResponse> updatePassword({
    Password password,
    String token,
  }) async {
    RequestResponse response;
    response = await UpdateUser.updatePassword(
      token: token,
      url: UPDATE_PASSWORD,
      body: password.toJson(),
    );
    return response;
  }
}
