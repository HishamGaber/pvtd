class TrackObject {
  String InstitutionTrackId;
  int order;
  TrackObject({this.InstitutionTrackId, this.order});

  Map<String, dynamic> toJson() => {
        "InstitutionTrackId": InstitutionTrackId,
        "order": order,
      };
}
