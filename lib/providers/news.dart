// To parse this JSON data, do
//
//     final news = newsFromJson(jsonString);

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:pvtd_app/common/api_urls.dart';

// NewsResponse newsFromJson(String str) =>
//     NewsResponse.fromJson(json.decode(str));

// String newsToJson(News data) => json.encode(data.toJson());

class NewsResponse with ChangeNotifier {
  NewsResponse({
    this.news,
    this.count,
  });

  List<News> news;
  int count;

  factory NewsResponse.fromJson(Map<String, dynamic> json) => NewsResponse(
        news: List<News>.from(json["news"].map((x) => News.fromJson(x))),
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "news": List<dynamic>.from(news.map((x) => x.toJson())),
        "count": count,
      };
}

class News with ChangeNotifier {
  NewsResponse newsResponse;
  News({
    this.id,
    this.title,
    this.coverImg,
    this.content,
    this.writer,
    this.views,
    this.active,
    this.updatedAt,
    this.institutionId,
    this.institution,
    this.tags,
    this.comments,
  });

  String id;
  String title;
  String coverImg;
  String content;
  dynamic writer;
  int views;
  bool active;
  DateTime updatedAt;
  String institutionId;
  Institution institution;
  List<Tag> tags;
  List<Comment> comments;

  factory News.fromJson(Map<String, dynamic> json) => News(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        coverImg: json["coverImg"] == null ? null : json["coverImg"],
        content: json["content"] == null ? null : json["content"],
        writer: json["writer"],
        views: json["views"] == null ? null : json["views"],
        active: json["active"] == null ? null : json["active"],
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        institutionId:
            json["InstitutionId"] == null ? null : json["InstitutionId"],
        institution: json["Institution"] == null
            ? null
            : Institution.fromJson(json["Institution"]),
        tags: json["Tags"] == null
            ? null
            : List<Tag>.from(json["Tags"].map((x) => Tag.fromJson(x))),
        comments: json["Comments"] == null
            ? null
            : List<Comment>.from(
                json["Comments"].map((x) => Comment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "coverImg": coverImg == null ? null : coverImg,
        "content": content == null ? null : content,
        "writer": writer,
        "views": views == null ? null : views,
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "InstitutionId": institutionId == null ? null : institutionId,
        "Institution": institution == null ? null : institution.toJson(),
        "Tags": tags == null
            ? null
            : List<dynamic>.from(tags.map((x) => x.toJson())),
        "Comments": comments == null
            ? null
            : List<dynamic>.from(comments.map((x) => x.toJson())),
      };
  Future<void> fetchAndSetNews() async {
    try {
      final response = await http.get(Uri.parse(MAIN_URL + NEWS_URL));
      newsResponse = NewsResponse.fromJson(json.decode(response.body));
    } catch (e) {
      print(' ' + e.toString());
    }
  }
}

class Comment {
  Comment({
    this.id,
    this.email,
    this.comment,
    this.name,
    this.status,
    this.reasonOfRejection,
    this.createdAt,
    this.updatedAt,
    this.newsId,
  });

  String id;
  String email;
  String comment;
  String name;
  String status;
  dynamic reasonOfRejection;
  DateTime createdAt;
  DateTime updatedAt;
  String newsId;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json["id"],
        email: json["email"],
        comment: json["comment"],
        name: json["name"],
        status: json["status"],
        reasonOfRejection: json["reasonOfRejection"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        newsId: json["NewsId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "comment": comment,
        "name": name,
        "status": status,
        "reasonOfRejection": reasonOfRejection,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "NewsId": newsId,
      };
}

class Institution {
  Institution({
    this.id,
    this.arTitle,
    this.enTitle,
    this.arAddress,
    this.enAddress,
    this.enMission,
    this.arMission,
    this.institutionType,
    this.logoName,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.regionId,
    this.governorateId,
  });

  String id;
  String arTitle;
  String enTitle;
  String arAddress;
  String enAddress;
  String enMission;
  String arMission;
  String institutionType;
  String logoName;
  bool active;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;
  String regionId;
  String governorateId;

  factory Institution.fromJson(Map<String, dynamic> json) => Institution(
        id: json["id"],
        arTitle: json["arTitle"],
        enTitle: json["enTitle"],
        arAddress: json["arAddress"],
        enAddress: json["enAddress"],
        enMission: json["enMission"],
        arMission: json["arMission"],
        institutionType: json["institutionType"],
        logoName: json["logoName"],
        active: json["active"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        regionId: json["RegionId"],
        governorateId: json["GovernorateId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "arTitle": arTitle,
        "enTitle": enTitle,
        "arAddress": arAddress,
        "enAddress": enAddress,
        "enMission": enMission,
        "arMission": arMission,
        "institutionType": institutionType,
        "logoName": logoName,
        "active": active,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "deletedAt": deletedAt,
        "RegionId": regionId,
        "GovernorateId": governorateId,
      };
}

class Tag {
  Tag({
    this.id,
    this.name,
  });

  String id;
  String name;

  factory Tag.fromJson(Map<String, dynamic> json) => Tag(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
