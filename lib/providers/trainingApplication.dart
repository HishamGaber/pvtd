import 'package:flutter/material.dart';
import 'package:pvtd_app/providers/trackObject.dart';

// import 'Track.dart';

class TrainingApplication {
  TrainingApplication({
    @required this.recruitingCardNumber,
    @required this.recruitingGovernorateId,
    @required this.graduationDate,
    @required this.certificate,
    @required this.language,
    @required this.grade,
    @required this.computerBirthCertificate,
    @required this.originalPrepCertificate,
    @required this.InstitutionTracks,
  });

  String recruitingCardNumber;
  String recruitingGovernorateId;
  String graduationDate;
  String certificate;
  String language;
  String grade;
  String computerBirthCertificate;
  String originalPrepCertificate;
  List<TrackObject> InstitutionTracks;

  factory TrainingApplication.fromJson(Map<String, dynamic> json) =>
      TrainingApplication(
        recruitingCardNumber: json["recruitingCardNumber"],
        recruitingGovernorateId: json["recruitingGovernorateId"],
        graduationDate: json["graduationDate"],
        certificate: json["certificate"],
        language: json["language"],
        grade: json["grade"],
        computerBirthCertificate: json["computerBirthCertificate"],
        originalPrepCertificate: json["originalPrepCertificate"],
        InstitutionTracks: List<TrackObject>.from(
            json["InstitutionTracks"].map((x) => InstitutionTrack.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "recruitingCardNumber": recruitingCardNumber,
        "recruitingGovernorateId": recruitingGovernorateId,
        "graduationDate": graduationDate,
        "certificate": certificate,
        "language": language,
        "grade": grade,
        "computerBirthCertificate": computerBirthCertificate,
        "originalPrepCertificate": originalPrepCertificate,
        "InstitutionTracks":
            List<dynamic>.from(InstitutionTracks.map((x) => x.toJson())),
      };
}

class InstitutionTrack {
  InstitutionTrack({
    this.institutionTrackId,
    this.order,
  });

  String institutionTrackId;
  int order;

  factory InstitutionTrack.fromJson(Map<String, dynamic> json) =>
      InstitutionTrack(
        institutionTrackId: json["InstitutionTrackId"],
        order: json["order"],
      );

  Map<String, dynamic> toJson() => {
        "InstitutionTrackId": institutionTrackId,
        "order": order,
      };
}
