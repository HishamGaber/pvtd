import 'package:flutter/material.dart';

class Student {
  Student({
    @required this.username,
    @required this.birthday,
    @required this.placeOfBirth,
    @required this.address,
    @required this.department,
    @required this.nationalId,
    @required this.civilRegistryOffice,
    @required this.phone,
    @required this.landline,
    @required this.email,
  });

  String username;
  String birthday;
  String placeOfBirth;
  String address;
  String department;
  String nationalId;
  String civilRegistryOffice;
  String phone;
  String landline;
  String email;

  factory Student.fromJson(Map<String, dynamic> json) => Student(
        username: json["username"],
        birthday: json["birthday"],
        placeOfBirth: json["placeOfBirth"],
        address: json["address"],
        department: json["department"],
        nationalId: json["nationalId"],
        civilRegistryOffice: json["civilRegistryOffice"],
        phone: json["phone"],
        landline: json["landline"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "birthday": birthday,
        "placeOfBirth": placeOfBirth,
        "address": address,
        "department": department,
        "nationalId": nationalId,
        "civilRegistryOffice": civilRegistryOffice,
        "phone": phone,
        "landline": landline,
        "email": email,
      };
}
