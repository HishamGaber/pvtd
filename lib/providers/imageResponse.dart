class ImageResponse {
  String fileUrl;
  String fileName;

  ImageResponse({
    this.fileUrl,
    this.fileName,
  });
  static fromJson(Map<String, dynamic> json) => ImageResponse(
        fileUrl: json["fileUrl"],
        fileName: json["fileName"],
      );

  Map<String, dynamic> toJson() => {
        "fileUrl": fileUrl,
        "fileName": fileName,
      };
}
