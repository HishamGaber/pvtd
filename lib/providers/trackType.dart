// To parse this JSON data, do
//
//     final trackTypeResponse = trackTypeResponseFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pvtd_app/network/fetch_api.dart';

TrackTypeResponse trackTypeResponseFromJson(String str) =>
    TrackTypeResponse.fromJson(json.decode(str));

String trackTypeResponseToJson(TrackTypeResponse data) =>
    json.encode(data.toJson());

class TrackTypeResponse {
  TrackTypeResponse({
    this.count,
    this.trackTypes,
  });

  int count;
  List<TrackType> trackTypes;

  factory TrackTypeResponse.fromJson(Map<String, dynamic> json) =>
      TrackTypeResponse(
        count: json["count"],
        trackTypes: List<TrackType>.from(
            json["trackTypes"].map((x) => TrackType.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "trackTypes": List<dynamic>.from(trackTypes.map((x) => x.toJson())),
      };
}

class TrackType with ChangeNotifier {
  TrackTypeResponse trackTypeResponse;
  List<TrackType> _trackTypes;
  List<String> _tracksIdMenu;
  // List<String> _trackStrings;
  List<TrackType> get trackTypes => _trackTypes;
  List<String> get tracksIdMenu => _tracksIdMenu;
  // List<String> get trackStrings => _trackStrings;
  TrackType({
    this.id,
    this.arTitle,
    this.enTitle,
    this.active,
    this.deletedAt,
  });

  String id;
  String arTitle;
  String enTitle;
  bool active;
  dynamic deletedAt;

  factory TrackType.fromJson(Map<String, dynamic> json) => TrackType(
        id: json["id"],
        arTitle: json["arTitle"],
        enTitle: json["enTitle"],
        active: json["active"],
        deletedAt: json["deletedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "arTitle": arTitle,
        "enTitle": enTitle,
        "active": active,
        "deletedAt": deletedAt,
      };

  fetchTrackTypes() async {
    _trackTypes = [];
    trackTypeResponse = await FetchApi.fetchAndSetTrackTypes();
    for (int i = 0; i < trackTypeResponse.trackTypes.length; i++) {
      _trackTypes.add(trackTypeResponse.trackTypes[i]);
    }

    notifyListeners();
  }
}
