import 'common_response.dart';

class AuthResponse extends CommonResponse {
  final User userData;

  AuthResponse(
    String message,
    this.userData,
  ) : super(
          message,
        );

  AuthResponse.fromMap(Map<String, dynamic> map)
      : userData = map["user"] == null
            ? null
            : User.fromMap(
                map["user"],
              ),
        super(
          map['error'],
        );

  AuthResponse.onError()
      : userData = null,
        super('حدث خطأ أثناء تنفيذ طلبك!');
}

class User {
  String role;
  int userId;
  String username;
  String firstName;
  String lastName;
  String email;
  String token;
  int expirationDate;
  dynamic companyProfile;
  dynamic studentProfile;
  dynamic individualProfile;
  int id;

  User({
    this.role,
    this.email,
    this.expirationDate,
    this.firstName,
    this.lastName,
    this.token,
    this.userId,
    this.username,
    this.companyProfile,
    this.individualProfile,
    this.studentProfile,
    this.id,
  });
  factory User.fromMap(Map<String, dynamic> user) => User(
        email: user['email'],
        token: user['token'],
        expirationDate: user['expirationDate'],
        username: user['username'],
        companyProfile: user['CompanyProfile'],
        studentProfile: user['StudentProfile'],
        individualProfile: user['IndividualProfile'],
        id: user['id'],
      );
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map["email"] = this.email;
    map["expirationDate"] = this.expirationDate;
    map["username"] = this.username;
    map["token"] = this.token;
    return map;
  }
}
