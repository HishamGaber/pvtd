import 'package:flutter/material.dart';

class Maintenance {
  Maintenance({
    @required this.address,
    @required this.description,
    @required this.paymentMethod,
    @required this.institutionId,
  });

  String address;
  String description;
  String paymentMethod;
  String institutionId;

  factory Maintenance.fromJson(Map<String, dynamic> json) => Maintenance(
        address: json["address"],
        description: json["description"],
        paymentMethod: json["paymentMethod"],
        institutionId: json["InstitutionId"],
      );

  Map<String, dynamic> toJson() => {
        "address": address,
        "description": description,
        "paymentMethod": paymentMethod,
        "InstitutionId": institutionId,
      };
}
