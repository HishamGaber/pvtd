import 'package:flutter/material.dart';

class Station {
  Station({
    @required this.industrialRegistry,
    @required this.commercialRegister,
    @required this.taxCard,
    @required this.titlingContract,
    @required this.trackTypes,
  });

  String industrialRegistry;
  String commercialRegister;
  String taxCard;
  String titlingContract;
  List<String> trackTypes;

  factory Station.fromJson(Map<String, dynamic> json) => Station(
        industrialRegistry: json["industrialRegistry"],
        commercialRegister: json["commercialRegister"],
        taxCard: json["taxCard"],
        titlingContract: json["titlingContract"],
        trackTypes: List<String>.from(json["trackTypes"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "industrialRegistry": industrialRegistry,
        "commercialRegister": commercialRegister,
        "taxCard": taxCard,
        "titlingContract": titlingContract,
        "trackTypes": List<dynamic>.from(trackTypes.map((x) => x)),
      };
}
