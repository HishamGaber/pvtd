import 'package:flutter/material.dart';

class Individual {
  Individual({
    @required this.email,
    @required this.username,
    @required this.nationalId,
    @required this.governorateId,
    @required this.address,
    @required this.phone,
    @required this.landline,
  });

  String email;
  String username;
  String nationalId;
  String governorateId;
  String address;
  String phone;
  String landline;

  factory Individual.fromJson(Map<String, dynamic> json) => Individual(
        email: json["email"],
        username: json["username"],
        nationalId: json["nationalId"],
        governorateId: json["GovernorateId"],
        address: json["address"],
        phone: json["phone"],
        landline: json["landline"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "username": username,
        "nationalId": nationalId,
        "GovernorateId": governorateId,
        "address": address,
        "phone": phone,
        "landline": landline,
      };
}
