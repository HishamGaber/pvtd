class CommonResponse {
  final String message;

  CommonResponse(
    this.message,
  );

  @override
  String toString() {
    return 'Message: $message';
  }

  CommonResponse.fromMap(Map<String, dynamic> map) : message = map['error'];

  CommonResponse.onError() : message = 'حدث خطأ أثناء تنفيذ طلبك!';
}
