import 'package:flutter/material.dart';

class Training {
  Training({
    @required this.educationalQualification,
    @required this.educationalQualificationYear,
    @required this.educationalSpecialization,
    @required this.higherQualification,
    @required this.higherQualificationYear,
    @required this.higherSpecialization,
    @required this.companyName,
    @required this.companyActivity,
    @required this.companyPhone,
    @required this.fax,
    @required this.address,
    @required this.job,
    @required this.suggestedProgram,
    @required this.institutionTrackId,
  });

  String educationalQualification;
  String educationalQualificationYear;
  String educationalSpecialization;
  String higherQualification;
  String higherQualificationYear;
  String higherSpecialization;
  String companyName;
  String companyActivity;
  String companyPhone;
  String fax;
  String address;
  String job;
  String suggestedProgram;
  String institutionTrackId;

  factory Training.fromJson(Map<String, dynamic> json) => Training(
        educationalQualification: json["educationalQualification"],
        educationalQualificationYear: json["educationalQualificationYear"],
        educationalSpecialization: json["educationalSpecialization"],
        higherQualification: json["higherQualification"],
        higherQualificationYear: json["higherQualificationYear"],
        higherSpecialization: json["higherSpecialization"],
        companyName: json["companyName"],
        companyActivity: json["companyActivity"],
        companyPhone: json["companyPhone"],
        fax: json["fax"],
        address: json["address"],
        job: json["job"],
        suggestedProgram: json["suggestedProgram"],
        institutionTrackId: json["InstitutionTrackId"],
      );

  Map<String, dynamic> toJson() => {
        "educationalQualification": educationalQualification,
        "educationalQualificationYear": educationalQualificationYear,
        "educationalSpecialization": educationalSpecialization,
        "higherQualification": higherQualification,
        "higherQualificationYear": higherQualificationYear,
        "higherSpecialization": higherSpecialization,
        "companyName": companyName,
        "companyActivity": companyActivity,
        "companyPhone": companyPhone,
        "fax": fax,
        "address": address,
        "job": job,
        "suggestedProgram": suggestedProgram,
        "InstitutionTrackId": institutionTrackId,
      };
}
