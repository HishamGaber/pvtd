import 'package:flutter/material.dart';

class Company {
  Company({
    @required this.username,
    @required this.nationalId,
    @required this.governorateId,
    @required this.commercialRegister,
    @required this.companyName,
    @required this.address,
    @required this.email,
    @required this.fax,
    @required this.phone,
    @required this.landline,
  });

  String username;
  String nationalId;
  String governorateId;
  String commercialRegister;
  String companyName;
  String address;
  String email;
  String fax;
  String phone;
  String landline;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
        username: json["username"],
        nationalId: json["nationalId"],
        governorateId: json["GovernorateId"],
        commercialRegister: json["commercialRegister"],
        companyName: json["companyName"],
        address: json["address"],
        email: json["email"],
        fax: json["fax"],
        phone: json["phone"],
        landline: json["landline"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "nationalId": nationalId,
        "GovernorateId": governorateId,
        "commercialRegister": commercialRegister,
        "companyName": companyName,
        "address": address,
        "email": email,
        "fax": fax,
        "phone": phone,
        "landline": landline,
      };
}
