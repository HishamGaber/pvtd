// To parse this JSON data, do
//
//     final userById = userByIdFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pvtd_app/common/api_urls.dart';

// UserById userByIdFromJson(String str) => UserById.fromJson(json.decode(str));

// String userByIdToJson(UserById data) => json.encode(data.toJson());

class UserById with ChangeNotifier {
  UserById userById;
  Future<void> getUserById(
    int id,
    String token,
  ) async {
    // try {
    final response = await http.get(
      Uri.parse(MAIN_URL + USERS + '$id'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
        'Content-Type': 'application/json',
      },
    );
    print('response ${response.body}');
    userById = UserById.fromJson(json.decode(response.body));
    notifyListeners();
    print('user  ' + userById.user.email);
    // } catch (e) {
    //   print('hisham ' + e.toString());
    // }
  }

  UserById({
    this.user,
  });

  UserResponse user;

  factory UserById.fromJson(Map<String, dynamic> json) => UserById(
        user: UserResponse.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
      };
}

class UserResponse with ChangeNotifier {
  UserResponse({
    this.id,
    this.firstName,
    this.lastName,
    this.avatar,
    this.phone,
    this.nationalId,
    this.email,
    this.username,
    this.name,
    this.role,
    this.password,
    this.active,
    this.address,
    this.landLine,
    this.restToken,
    this.restTokenExpireAt,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.governorateId,
    this.institutionId,
    this.governorate,
    this.userRoles,
    this.companyProfile,
    this.studentProfile,
    this.individualProfile,
  });

  int id;
  dynamic firstName;
  dynamic lastName;
  dynamic avatar;
  dynamic phone;
  dynamic nationalId;
  String email;
  dynamic username;
  String name;
  String role;
  String password;
  bool active;
  dynamic address;
  dynamic landLine;
  dynamic restToken;
  dynamic restTokenExpireAt;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;
  dynamic governorateId;
  dynamic institutionId;
  List<dynamic> userRoles;
  CompanyProfile companyProfile;
  StudentProfile studentProfile;
  IndividualProfile individualProfile;
  Governorate governorate;

  factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
        id: json["id"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        avatar: json["avatar"],
        phone: json["phone"],
        nationalId: json["nationalId"],
        email: json["email"],
        username: json["username"],
        name: json["name"],
        role: json["role"],
        password: json["password"],
        active: json["active"],
        address: json["address"],
        landLine: json["landLine"],
        restToken: json["restToken"],
        restTokenExpireAt: json["restTokenExpireAt"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        governorateId: json["GovernorateId"],
        institutionId: json["InstitutionId"],
        governorate: json["Governorate"] != null
            ? Governorate.fromJson(json["Governorate"])
            : null,
        userRoles: List<dynamic>.from(json["UserRoles"].map((x) => x)),
        companyProfile: json["CompanyProfile"] != null
            ? CompanyProfile.fromJson(json["CompanyProfile"])
            : null,
        studentProfile: json["StudentProfile"] != null
            ? StudentProfile.fromJson(json["StudentProfile"])
            : null,
        individualProfile: json["IndividualProfile"] != null
            ? IndividualProfile.fromJson(json["IndividualProfile"])
            : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "avatar": avatar,
        "phone": phone,
        "nationalId": nationalId,
        "email": email,
        "username": username,
        "name": name,
        "role": role,
        "password": password,
        "active": active,
        "address": address,
        "landLine": landLine,
        "restToken": restToken,
        "restTokenExpireAt": restTokenExpireAt,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "deletedAt": deletedAt,
        "GovernorateId": governorateId,
        "InstitutionId": institutionId,
        "Governorate": governorate.toJson(),
        "UserRoles": List<dynamic>.from(userRoles.map((x) => x)),
        "CompanyProfile": companyProfile.toJson(),
        "StudentProfile": studentProfile.toJson(),
        "IndividualProfile": individualProfile.toJson(),
      };
}

class CompanyProfile {
  CompanyProfile({
    this.id,
    this.companyName,
    this.commercialRegister,
    this.fax,
    this.createdAt,
    this.updatedAt,
    this.userId,
  });

  String id;
  String companyName;
  dynamic commercialRegister;
  dynamic fax;
  DateTime createdAt;
  DateTime updatedAt;
  int userId;

  factory CompanyProfile.fromJson(Map<String, dynamic> json) => CompanyProfile(
        id: json["id"],
        companyName: json["companyName"],
        commercialRegister: json["commercialRegister"],
        fax: json["fax"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        userId: json["UserId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "companyName": companyName,
        "commercialRegister": commercialRegister,
        "fax": fax,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "UserId": userId,
      };
}

class IndividualProfile {
  IndividualProfile({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.userId,
  });

  String id;
  DateTime createdAt;
  DateTime updatedAt;
  int userId;

  factory IndividualProfile.fromJson(Map<String, dynamic> json) =>
      IndividualProfile(
        id: json["id"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        userId: json["UserId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "UserId": userId,
      };
}

class Governorate {
  Governorate({
    this.id,
    this.arTitle,
    this.enTitle,
    this.active,
  });

  String id;
  String arTitle;
  String enTitle;
  bool active;

  factory Governorate.fromJson(Map<String, dynamic> json) => Governorate(
        id: json["id"],
        arTitle: json["arTitle"],
        enTitle: json["enTitle"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "arTitle": arTitle,
        "enTitle": enTitle,
        "active": active,
      };
}

class StudentProfile {
  StudentProfile({
    this.id,
    this.birthday,
    this.placeOfBirth,
    this.civilRegistryOffice,
    this.department,
    this.createdAt,
    this.updatedAt,
    this.userId,
    this.civilRegistryGovernorateId,
  });

  String id;
  dynamic birthday;
  dynamic placeOfBirth;
  dynamic civilRegistryOffice;
  dynamic department;
  DateTime createdAt;
  DateTime updatedAt;
  int userId;
  dynamic civilRegistryGovernorateId;

  factory StudentProfile.fromJson(Map<String, dynamic> json) => StudentProfile(
        id: json["id"],
        birthday:
            json["birthday"] != null ? DateTime.parse(json["birthday"]) : null,
        placeOfBirth: json["placeOfBirth"],
        civilRegistryOffice: json["civilRegistryOffice"],
        department: json["department"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        userId: json["UserId"],
        civilRegistryGovernorateId: json["civilRegistryGovernorateId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "birthday": birthday,
        "placeOfBirth": placeOfBirth,
        "civilRegistryOffice": civilRegistryOffice,
        "department": department,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "UserId": userId,
        "civilRegistryGovernorateId": civilRegistryGovernorateId,
      };
}
