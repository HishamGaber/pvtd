import 'package:flutter/material.dart';

class Skill {
  Skill({
    @required this.entity,
    @required this.graduationYear,
    @required this.educationalQualification,
    @required this.previousExperience,
    @required this.hasExperience,
    @required this.institutionId,
    @required this.institutionTrackId,
  });

  String entity;
  String graduationYear;
  String educationalQualification;
  String previousExperience;
  bool hasExperience;
  String institutionId;
  String institutionTrackId;

  factory Skill.fromJson(Map<String, dynamic> json) => Skill(
        entity: json["entity"],
        graduationYear: json["graduationYear"],
        educationalQualification: json["educationalQualification"],
        previousExperience: json["previousExperience"],
        hasExperience: json["hasExperience"],
        institutionId: json["InstitutionId"],
        institutionTrackId: json["InstitutionTrackId"],
      );

  Map<String, dynamic> toJson() => {
        "entity": entity,
        "graduationYear": graduationYear,
        "educationalQualification": educationalQualification,
        "previousExperience": previousExperience,
        "hasExperience": hasExperience,
        "InstitutionId": institutionId,
        "InstitutionTrackId": institutionTrackId,
      };
}
