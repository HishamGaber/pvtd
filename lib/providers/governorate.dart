// To parse this JSON data, do
//
//     final userById = userByIdFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pvtd_app/network/fetch_api.dart';

GovernorateResponse userByIdFromJson(String str) =>
    GovernorateResponse.fromJson(json.decode(str));

String userByIdToJson(GovernorateResponse data) => json.encode(data.toJson());

class GovernorateResponse {
  GovernorateResponse({
    this.count,
    this.governorates,
  });

  int count;
  List<Governorate> governorates;

  factory GovernorateResponse.fromJson(Map<String, dynamic> json) =>
      GovernorateResponse(
        count: json["count"],
        governorates: List<Governorate>.from(
            json["governorates"].map((x) => Governorate.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "governorates": List<dynamic>.from(governorates.map((x) => x.toJson())),
      };
}

class Governorate with ChangeNotifier {
  GovernorateResponse governorateResponse;
  List<Governorate> governorates = [];
  Governorate({
    this.id,
    this.arTitle,
    this.enTitle,
    this.active,
  });

  String id;
  String arTitle;
  String enTitle;
  bool active;

  factory Governorate.fromJson(Map<String, dynamic> json) => Governorate(
        id: json["id"],
        arTitle: json["arTitle"],
        enTitle: json["enTitle"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "arTitle": arTitle,
        "enTitle": enTitle,
        "active": active,
      };

  fetchAndSetGovernorates() async {
    governorateResponse = await FetchApi.fetchAndSetGovernorates();
    for (int i = 0; i < governorateResponse.governorates.length; i++) {
      governorates.add(governorateResponse.governorates[i]);
    }
    notifyListeners();
  }
}
