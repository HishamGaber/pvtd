import 'package:pvtd_app/providers/common_response.dart';

class RequestResponse extends CommonResponse {
  RequestResponse(String message, this.requestData) : super(message);
  final Request requestData;

  RequestResponse.fromMap(Map<String, dynamic> map)
      : requestData = map["user"] == null
            ? null
            : Request.fromMap(
                map["user"],
              ),
        super(
          map['error'],
        );

  RequestResponse.onError()
      : requestData = null,
        super('حدث خطأ أثناء تنفيذ طلبك!');
}

class Request {
  int id;
  String state;
  String topic;
  String target;
  String otherData;
  int UserId;
  String updatedAt;
  String createdAt;

  Request({
    this.id,
    this.state,
    this.topic,
    this.target,
    this.otherData,
    this.UserId,
    this.updatedAt,
    this.createdAt,
  });
  factory Request.fromMap(Map<String, dynamic> request) => Request(
        id: request['id'],
        state: request['state'],
        topic: request['topic'],
        target: request['targete'],
        otherData: request['otherData'],
        UserId: request['UserId'],
        updatedAt: request['updatedAt'],
        createdAt: request['createdAt'],
      );
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map["id"] = this.id;
    map["state"] = this.state;
    map["topic"] = this.topic;
    map["target"] = this.target;
    map["otherData"] = this.otherData;
    map["UserId"] = this.UserId;
    map["updatedAt"] = this.updatedAt;
    map["createdAt"] = this.createdAt;
    return map;
  }
}
