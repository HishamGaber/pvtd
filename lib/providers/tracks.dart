// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pvtd_app/widgets/programs.dart';
import '/common/api_urls.dart';
import 'package:http/http.dart' as http;
import '../network/fetch_api.dart';

// InstitutionTracksResponse welcomeFromJson(String str) =>
//     InstitutionTracksResponse.fromJson(json.decode(str));

// String welcomeToJson(InstitutionTracksResponse data) =>
//     json.encode(data.toJson());

class InstitutionTracksResponse {
  InstitutionTracksResponse({
    this.count,
    this.institutionTrainings,
  });

  int count;
  List<InstitutionTraining> institutionTrainings;

  factory InstitutionTracksResponse.fromJson(Map<String, dynamic> json) =>
      InstitutionTracksResponse(
        count: json["count"],
        institutionTrainings: List<InstitutionTraining>.from(
            json["institutionTrainings"]
                .map((x) => InstitutionTraining.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "institutionTrainings":
            List<dynamic>.from(institutionTrainings.map((x) => x.toJson())),
      };
}

class InstitutionTraining with ChangeNotifier {
  // List<String> menuInstitutions = [];
  List<InstitutionTraining> tracks = [];
  List<InstitutionTraining> tracksOfInstitution = [];
  List<InstitutionTraining> programs = [];

  InstitutionTraining dropdownValueTrack;

  InstitutionTracksResponse institutionsTracksResponse;
  // set tracksMenuInstitution(List<String> value) {
  //   this._tracksMenuInstitution = value;
  // }

  // List<String> get tracksMenuInstitution => _tracksMenuInstitution;
  InstitutionTraining({
    this.id,
    this.arDescription,
    this.enDescription,
    this.skills,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.eductionTrackId,
    this.institutionId,
    this.eductionTrack,
  });

  String id;
  String arDescription;
  String enDescription;
  String skills;
  dynamic active;
  DateTime createdAt;
  DateTime updatedAt;
  String eductionTrackId;
  String institutionId;
  EductionTrack eductionTrack;

  factory InstitutionTraining.fromJson(Map<String, dynamic> json) =>
      InstitutionTraining(
        id: json["id"],
        arDescription: json["arDescription"],
        enDescription: json["enDescription"],
        skills: json["skills"],
        active: json["active"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        eductionTrackId: json["EductionTrackId"],
        institutionId: json["InstitutionId"],
        eductionTrack: EductionTrack.fromJson(json["EductionTrack"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "arDescription": arDescription,
        "enDescription": enDescription,
        "skills": skills,
        "active": active,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "EductionTrackId": eductionTrackId,
        "InstitutionId": institutionId,
        "EductionTrack": eductionTrack.toJson(),
      };

  // Future<InstitutionTracksResponse> fetchAndSetTracksInstitute(
  //     String instituteID) async {
  //   final response = await http
  //       .get(Uri.parse(MAIN_URL + TRACKS_INSTITUTION_URL + '$instituteID'));

  //   institutionsTracksResponse =
  //       InstitutionTracksResponse.fromJson(json.decode(response.body));

  //   return institutionsTracksResponse;
  //   // notifyListeners();
  // }

  fetchAndSetTracks() async {
    InstitutionTracksResponse response = await FetchApi.fetchAndSetTracks();
    // for (int i = 0; i < response.institutionTrainings.length; i++) {
    tracks = response.institutionTrainings;
    // }
    notifyListeners();
  }

  // Future<List<InstitutionTraining>>
  fetchAndSetTracksByInstitution(String institutionId) async {
    tracksOfInstitution = [];
    InstitutionTracksResponse response =
        await FetchApi.fetchAndSetTracksByInstitution(institutionId);
    // for (int i = 0; i < response.institutionTrainings.length; i++) {
    tracksOfInstitution = response.institutionTrainings;
    // }
    // return tracksMenuInstitution;
    notifyListeners();
    // return tracksOfInstitution;
  }

  fetchAndSetPrograms(String educationTrackId) async {
    programs = [];
    InstitutionTracksResponse response =
        await FetchApi.fetchAndSetPrograms(educationTrackId);
    programs = response.institutionTrainings;

    notifyListeners();
  }

  clearTracks() {
    tracksOfInstitution.clear();
    notifyListeners();
  }

  void removeEducationTrack(String eductionTrackId) {
    dropdownValueTrack = null;
    notifyListeners();
    // tracksOfInstitution.removeLast();
    // print(tracksOfInstitution.join(','));
    // // tracksOfInstitution = [...tracksOfInstitution];
    // notifyListeners();
  }
}

class EductionTrack {
  EductionTrack({
    this.enTitle,
    this.arTitle,
    this.trackType,
  });

  String enTitle;
  String arTitle;
  TrackType trackType;

  factory EductionTrack.fromJson(Map<String, dynamic> json) => EductionTrack(
        enTitle: json["enTitle"],
        arTitle: json["arTitle"],
        trackType: json['TrackType'] != null
            ? TrackType.fromJson(json['TrackType'])
            : null,
      );

  Map<String, dynamic> toJson() => {
        "enTitle": enTitle,
        "arTitle": arTitle,
        'TrackType': TrackType,
      };
}

class TrackType {
  TrackType({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.arTitle,
  });

  String id;
  DateTime createdAt;
  DateTime updatedAt;
  String arTitle;

  factory TrackType.fromJson(Map<String, dynamic> json) => TrackType(
        id: json["id"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        arTitle: json['arTitle'],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "arTitle": arTitle,
      };
}
