// To parse this JSON data, do
//
//     final services = servicesFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pvtd_app/common/api_urls.dart';

// Services servicesFromJson(String str) => Services.fromJson(json.decode(str));

// String servicesToJson(Services data) => json.encode(data.toJson());

class ServicesResponse {
  ServicesResponse({
    this.count,
    this.services,
  });

  int count;
  List<Service> services;

  factory ServicesResponse.fromJson(Map<String, dynamic> json) =>
      ServicesResponse(
        count: json["count"],
        services: List<Service>.from(
            json["services"].map((x) => Service.fromJson(x))),
      );

  // Map<String, dynamic> toJson() => {
  //     "count": count,
  //     "services": List<dynamic>.from(services.map((x) => x.toJson())),
  // };
}

class Service with ChangeNotifier {
  ServicesResponse serviceResponse;
  Service({
    this.audiences,
    this.id,
    this.arTitle,
    this.enTitle,
    this.arDescription,
    this.enDescription,
    this.showInProfile,
    this.serviceType,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  List<Audience> audiences;
  String id;
  String arTitle;
  String enTitle;
  String arDescription;
  String enDescription;
  bool showInProfile;
  String serviceType;
  bool active;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;

  factory Service.fromJson(Map<String, dynamic> json) => Service(
        audiences: List<Audience>.from(
            json["audiences"].map((x) => audienceValues.map[x])),
        id: json["id"],
        arTitle: json["arTitle"],
        enTitle: json["enTitle"],
        arDescription: json["arDescription"],
        enDescription: json["enDescription"],
        showInProfile: json["showInProfile"],
        serviceType: json["serviceType"],
        active: json["active"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
      );

  // Map<String, dynamic> toJson() => {
  //     "audiences": List<dynamic>.from(audiences.map((x) => audienceValues.reverse[x])),
  //     "id": id,
  //     "arTitle": arTitle,
  //     "enTitle": enTitle,
  //     "arDescription": arDescription,
  //     "enDescription": enDescription,
  //     "showInProfile": showInProfile,
  //     "serviceType": serviceType,
  //     "active": active,
  //     "createdAt": createdAt.toIso8601String(),
  //     "updatedAt": updatedAt.toIso8601String(),
  //     "deletedAt": deletedAt,
  // };

  Future<void> fetchServices() async {
    Uri url = Uri.parse(MAIN_URL + SERVICES_URL);
    final response = await http.get(url);
    serviceResponse = ServicesResponse.fromJson(json.decode(response.body));
  }
}

enum Audience { INDIVIDUAL, COMPANIES, STUDENTS }

final audienceValues = EnumValues({
  "companies": Audience.COMPANIES,
  "individual": Audience.INDIVIDUAL,
  "students": Audience.STUDENTS
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
