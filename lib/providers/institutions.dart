// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:pvtd_app/network/fetch_api.dart';

// InstitutionsResponse welcomeFromJson(String str) =>
//     InstitutionsResponse.fromJson(json.decode(str));

// String welcomeToJson(InstitutionsResponse data) => json.encode(data.toJson());

class InstitutionsResponse {
  InstitutionsResponse({
    this.count,
    this.institutions,
  });

  int count;
  List<Institution> institutions;
  var institutionsMenu;

  factory InstitutionsResponse.fromJson(Map<String, dynamic> json) =>
      InstitutionsResponse(
        count: json["count"],
        institutions: List<Institution>.from(
            json["institutions"].map((x) => Institution.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "institutions": List<dynamic>.from(institutions.map((x) => x.toJson())),
      };
}

class Institution with ChangeNotifier {
  InstitutionsResponse institutionsResponse;
  List<Institution> _institutions = [];
  List<String> menuInstitutions = [];

  List<Institution> get institutions => _institutions;
  // set institutionsMenu(List<String> value) {
  //   this._institutionsMenu = value;
  // }

  Institution({
    this.id,
    this.arTitle,
    this.enTitle,
    this.arAddress,
    this.enAddress,
    this.enMission,
    this.arMission,
    this.institutionType,
    this.logoName,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.regionId,
    this.governorateId,
    this.institutionPhones,
    this.institutionStatistics,
    this.institutionLinks,
    this.region,
  });

  String id;
  String arTitle;
  String enTitle;
  String arAddress;
  String enAddress;
  String enMission;
  String arMission;
  String institutionType;
  String logoName;
  bool active;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;
  dynamic regionId;
  dynamic governorateId;
  List<InstitutionPhone> institutionPhones;
  List<InstitutionStatistic> institutionStatistics;
  List<InstitutionLink> institutionLinks;
  dynamic region;

  factory Institution.fromJson(Map<String, dynamic> json) => Institution(
        id: json["id"] == null ? null : json["id"],
        arTitle: json["arTitle"] == null ? null : json["arTitle"],
        enTitle: json["enTitle"] == null ? null : json["enTitle"],
        arAddress: json["arAddress"] == null ? null : json["arAddress"],
        enAddress: json["enAddress"] == null ? null : json["enAddress"],
        enMission: json["enMission"] == null ? null : json["enMission"],
        arMission: json["arMission"] == null ? null : json["arMission"],
        institutionType:
            json["institutionType"] == null ? null : json["institutionType"],
        logoName: json["logoName"] == null ? null : json["logoName"],
        active: json["active"] == null ? null : json["active"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        deletedAt: json["deletedAt"],
        regionId: json["RegionId"],
        governorateId: json["GovernorateId"],
        institutionPhones: json["InstitutionPhones"] == null
            ? null
            : List<InstitutionPhone>.from(json["InstitutionPhones"]
                .map((x) => InstitutionPhone.fromJson(x))),
        institutionStatistics: json["InstitutionStatistics"] == null
            ? null
            : List<InstitutionStatistic>.from(json["InstitutionStatistics"]
                .map((x) => InstitutionStatistic.fromJson(x))),
        institutionLinks: json["InstitutionLinks"] == null
            ? null
            : List<InstitutionLink>.from(json["InstitutionLinks"]
                .map((x) => InstitutionLink.fromJson(x))),
        region: json["Region"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "arTitle": arTitle == null ? null : arTitle,
        "enTitle": enTitle == null ? null : enTitle,
        "arAddress": arAddress == null ? null : arAddress,
        "enAddress": enAddress == null ? null : enAddress,
        "enMission": enMission == null ? null : enMission,
        "arMission": arMission == null ? null : arMission,
        "institutionType": institutionType == null ? null : institutionType,
        "logoName": logoName == null ? null : logoName,
        "active": active == null ? null : active,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "deletedAt": deletedAt,
        "RegionId": regionId,
        "GovernorateId": governorateId,
        "InstitutionPhones": institutionPhones == null
            ? null
            : List<dynamic>.from(institutionPhones.map((x) => x.toJson())),
        "InstitutionStatistics": institutionStatistics == null
            ? null
            : List<dynamic>.from(institutionStatistics.map((x) => x.toJson())),
        "InstitutionLinks": institutionLinks == null
            ? null
            : List<dynamic>.from(institutionLinks.map((x) => x.toJson())),
        "Region": region,
      };

  fetchAndSetInstitutions() async {
    _institutions = [];
    institutionsResponse = await FetchApi.fetchAndSetInstitutions();
    // for (int i = 0; i < institutionsResponse.institutions.length; i++) {
    _institutions = institutionsResponse.institutions;
    // }
    // dropdownValueInstitutions = institutionsMenu[0];
    notifyListeners();
  }

  // fetchAndSetProgramsByInstitution(String institutionId) async {
  //   InstitutionTracksResponse response =
  //       await InstitutionApi.fetchAndProgramsByInstitution(institutionId);
  //   for (int i = 0; i < response.institutionTrainings.length; i++) {
  //     menuInstitutions
  //         .add(response.institutionTrainings[i].eductionTrack.arTitle);
  //   }
  //   notifyListeners();
  // }

  Future<String> getInstitutionIdByName(String institutionName) async {
    InstitutionsResponse response = await FetchApi.fetchAndSetInstitutions();
    for (int i = 0; i < response.institutions.length; i++) {
      if (response.institutions[i].arTitle == institutionName) {
        return response.institutions[i].id;
      }
    }
  }

  searchfn(String keyword, items) {
    List<String> s = [];
    List<int> ret = List<int>();
    if (keyword != null && items != null && keyword.isNotEmpty) {
      keyword.split(" ").forEach((k) {
        int i = 0;
        items.forEach((item) {
          if (k.isNotEmpty &&
              (item.value.toString().toLowerCase().contains(
                    k.toLowerCase(),
                  ))) {
            ret.add(i);
          }
          i++;
        });
      });
    }
    if (keyword.isEmpty) {
      ret = Iterable<int>.generate(items.length).toList();
    }
    return (ret);
  }
}

class InstitutionLink {
  InstitutionLink({
    this.id,
    this.url,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.institutionId,
    this.linkTypeId,
  });

  String id;
  String url;
  bool active;
  DateTime createdAt;
  DateTime updatedAt;
  String institutionId;
  String linkTypeId;

  factory InstitutionLink.fromJson(Map<String, dynamic> json) =>
      InstitutionLink(
        id: json["id"],
        url: json["url"],
        active: json["active"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        institutionId: json["InstitutionId"],
        linkTypeId: json["LinkTypeId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "active": active,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "InstitutionId": institutionId,
        "LinkTypeId": linkTypeId,
      };
}

class InstitutionPhone {
  InstitutionPhone({
    this.id,
    this.number,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.institutionId,
    this.phoneTypeId,
  });

  String id;
  String number;
  bool active;
  DateTime createdAt;
  DateTime updatedAt;
  String institutionId;
  String phoneTypeId;

  factory InstitutionPhone.fromJson(Map<String, dynamic> json) =>
      InstitutionPhone(
        id: json["id"],
        number: json["number"],
        active: json["active"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        institutionId: json["InstitutionId"],
        phoneTypeId: json["PhoneTypeId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "number": number,
        "active": active,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "InstitutionId": institutionId,
        "PhoneTypeId": phoneTypeId,
      };
}

class InstitutionStatistic {
  InstitutionStatistic({
    this.id,
    this.arTitle,
    this.enTitle,
    this.value,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.institutionId,
    this.statisticTypeId,
  });

  String id;
  dynamic arTitle;
  dynamic enTitle;
  String value;
  bool active;
  DateTime createdAt;
  DateTime updatedAt;
  String institutionId;
  String statisticTypeId;

  factory InstitutionStatistic.fromJson(Map<String, dynamic> json) =>
      InstitutionStatistic(
        id: json["id"],
        arTitle: json["arTitle"],
        enTitle: json["enTitle"],
        value: json["value"],
        active: json["active"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        institutionId: json["InstitutionId"],
        statisticTypeId: json["StatisticTypeId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "arTitle": arTitle,
        "enTitle": enTitle,
        "value": value,
        "active": active,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "InstitutionId": institutionId,
        "StatisticTypeId": statisticTypeId,
      };
}
