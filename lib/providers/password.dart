import 'package:flutter/material.dart';

class Password {
  Password({
    @required this.password,
    @required this.currentPassword,
  });

  String password;
  String currentPassword;

  factory Password.fromJson(Map<String, dynamic> json) => Password(
        password: json["password"],
        currentPassword: json["currentPassword"],
      );

  Map<String, dynamic> toJson() => {
        "password": password,
        "currentPassword": currentPassword,
      };
}
