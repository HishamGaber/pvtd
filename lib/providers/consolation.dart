import 'package:flutter/material.dart';

class Consolation {
  Consolation({
    @required this.topic,
    @required this.target,
    @required this.otherData,
  });

  String topic;
  String target;
  String otherData;

  factory Consolation.fromJson(Map<String, dynamic> json) => Consolation(
        topic: json["topic"],
        target: json["target"],
        otherData: json["otherData"],
      );

  Map<String, dynamic> toJson() => {
        "topic": topic,
        "target": target,
        "otherData": otherData,
      };
}
