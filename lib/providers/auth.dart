import 'dart:convert';

import 'package:flutter/material.dart';
import '/network/production_user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'user.dart';

class Auth extends ChangeNotifier {
  bool success;
  // int _expirationDate;
  String _token;
  String _message;
  String _email;
  String _password;
  bool _isLogin;
  bool _isCompany = false;
  bool _isStudent = false;
  bool _isIndividual = false;
  int _id;
  bool get isLogin => _isLogin;
  String get message => _message;
  // int get expirationDate => _expirationDate;
  String get email => _email;
  String get password => _password;
  String get token => _token;
  bool get isCompany => _isCompany;
  bool get isStudent => _isStudent;
  bool get isIndividual => _isIndividual;
  int get id => _id;

  Future<bool> login(String email, String password) async {
    // print('email $_email ');
    // print('password $_password');
    AuthResponse response = await ProductionUser.login(email, password);
    if (response.message != null) {
      _message = response.message;
      // print('message $_message');
      success = false;

      notifyListeners();
    } else {
      if (response.userData.companyProfile != null) {
        _isCompany = true;
        notifyListeners();
      }
      if (response.userData.studentProfile != null) {
        _isStudent = true;
        notifyListeners();
      }
      if (response.userData.individualProfile != null) {
        _isIndividual = true;
        notifyListeners();
      }

      await saveLocalStorge(
        response.userData.email,
        response.userData.expirationDate,
        response.userData.token,
        response.userData.id,
        response.userData.companyProfile,
        response.userData.studentProfile,
        response.userData.individualProfile,
      );
      // _token = response.userData.token;
      // notifyListeners();
      // _expirationDate = response.userData.expirationDate;
      await isLoggedIn();

      success = true;
      notifyListeners();
    }
    return success;
  }

  Future<bool> registerStudent(
    String username,
    String email,
    String mobile,
    String nationalid,
    String password,
  ) async {
    AuthResponse response = await ProductionUser.signupStudent(
      username,
      email,
      mobile,
      nationalid,
      password,
    );
    if (response.message != null) {
      _message = response.message;
      success = false;
      notifyListeners();
    } else {
      success = true;
    }
    return success;
  }

  Future<bool> registerCompany(
    String companyName,
    String email,
    String password,
    String username,
  ) async {
    AuthResponse response = await ProductionUser.signupCompany(
        companyName, email, password, username);

    if (response.message != null) {
      _message = response.message;
      success = false;
      notifyListeners();
    } else {
      success = true;
    }
    return success;
  }

  Future<bool> registerIndividual(
    String email,
    String password,
    String username,
  ) async {
    AuthResponse response = await ProductionUser.signupIndividual(
      email,
      password,
      username,
    );

    if (response.message != null) {
      _message = response.message;
      success = false;
      notifyListeners();
    } else {
      success = true;
    }
    return success;
  }

  forgotPassword() {}
  Future<bool> logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    _isLogin = false;
    notifyListeners();
    return _isLogin;
    // isLoggedIn();
  }

  Future<void> saveLocalStorge(
    String email,
    int expirationDate,
    String token,
    int id,
    dynamic companyProfile,
    dynamic studentProfile,
    dynamic individualProfile,
  ) async {
    // save data at local storge
    // go to home view
    final prefs = await SharedPreferences.getInstance();
    final emailKey = 'email';
    final emailValue = email;
    final expiredKey = 'expirationDate';
    final expiredValue = expirationDate;
    final tokenKey = 'token';
    final tokenValue = token;
    final idKey = 'id';
    final idValue = id;
    prefs.setString(emailKey, emailValue);
    prefs.setInt(expiredKey, expiredValue);
    prefs.setString(tokenKey, tokenValue);
    prefs.setInt(idKey, idValue);
    prefs.setString('studentProfile', jsonEncode(studentProfile));
    prefs.setString('companyProfile', jsonEncode(companyProfile));
    prefs.setString('individualProfile', jsonEncode(individualProfile));
    print('saved ${prefs.getString('email')}');
  }

  Future<int> getId() async {
    final prefs = await SharedPreferences.getInstance();
    final idKey = 'id';
    final int _id = prefs.getInt(idKey);
    notifyListeners();
  }

  Future<bool> isLoggedIn() async {
    final prefs = await SharedPreferences.getInstance();

    final tokenkey = 'token';
    final expiredvalue = prefs.getInt('expirationDate') ?? 0;
    var now = DateTime.now().millisecondsSinceEpoch;

    final tokenvalue = prefs.getString(tokenkey) ?? null;
    final idValue = prefs.getInt('id');
    //companyProfile = prefs.get('companyProfile');

    _isCompany = prefs.get('companyProfile') != "null";
    _isStudent = prefs.get('studentProfile') != "null";
    _isIndividual = prefs.get('individualProfile') != "null";
    _id = idValue;
    print('token $tokenvalue');
    _token = tokenvalue;
    _isLogin = (tokenvalue != null) && expiredvalue > now;
    notifyListeners();
    return _isLogin;
  }
}
