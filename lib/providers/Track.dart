class Track {
  String title;
  String institutionTrackId;
  int order;

  Track(
    this.title,
    this.institutionTrackId,
    this.order,
  );
  Map toJson() => {"institutionTrackId": institutionTrackId, "order": order};
}
