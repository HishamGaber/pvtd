import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/screens/home_screen.dart';
import '/providers/auth.dart';

class Splash extends StatefulWidget {
  // static final navKey = new GlobalKey<NavigatorState>();
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  int expiryDate;
  var now;
  bool loggedin;
  Auth auth;
  @override
  void initState() {
    auth = Provider.of<Auth>(context, listen: false);
    super.initState();
    asyncMethod();
  }

  void asyncMethod() async {
    await auth.isLoggedIn();

    Duration ms = const Duration(milliseconds: 3);

    var duration = ms * 1000;
    Timer(duration, () {
      Navigator.pushReplacementNamed(context, HomeScreen.routeName);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('assets/images/bitmap.png'),
              ),
              Text(
                'مصلحة الكفاية الإنتاجية \n والتدريب المهني',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
