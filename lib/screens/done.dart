import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';

class Done extends StatelessWidget {
  static const routeName = '/done';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/images/success.png'),
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              'تم ارسال طلب التقديم بنجاح',
              style: const TextStyle(
                color: const Color(0xff343433),
                fontWeight: FontWeight.w700,
                fontFamily: "Almarai",
                fontStyle: FontStyle.normal,
                fontSize: 24.0,
              ),
            ),
            Text('سوف يتم اعلامك بالنتيجة في خانة طلباتي'),
            SizedBox(
              height: 25,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/myRequests');
              },
              child: Text('متابعة الطلب'),
            )
          ],
        ),
      ),
    );
  }
}
