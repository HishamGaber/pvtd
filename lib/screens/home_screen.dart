import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '/screens/myaccount_screen.dart';
import '/screens/news_screen.dart';
import '/screens/myrequests_screen.dart';
import '/screens/services_screen.dart';
import '/providers/auth.dart';
import 'myGrades.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/homeScreen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

TabController tabController;

// This is the private State class that goes with MyStatefulWidget.
class _HomeScreenState extends State<HomeScreen> {
  var bottomNavItems = <BottomNavigationBarItem>[
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: 'الخدمات',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.business),
      label: 'الاخبار',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.school),
      label: 'حسابي',
    ),
  ];
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    ServicesScreen(),
    NewsScreen(),
    Myaccount(),
    MyRequestsScreen(),
    MyGrades(),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Auth auth = Provider.of<Auth>(context, listen: false);
    if ((auth.isLogin) && bottomNavItems.length == 3) {
      bottomNavItems.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.notifications),
          label: 'طلباتي',
        ),
      );
    }
    if (auth.isStudent && bottomNavItems.length == 4) {
      bottomNavItems.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.grade_sharp),
          label: 'درجاتي',
        ),
      );
    }
    if (!(auth.isLogin) && bottomNavItems.length == 5) {
      print('jhdh');
      setState(() {
        bottomNavItems.removeLast();
        bottomNavItems.removeLast();
      });
    }
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Color.fromRGBO(45, 54, 147, 1),
        automaticallyImplyLeading: false,
        title: Center(
          child: Text(
            bottomNavItems.elementAt(_selectedIndex).label,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: List.of(bottomNavItems),
        currentIndex: _selectedIndex,
        selectedItemColor: Color.fromRGBO(45, 54, 147, 1),
        onTap: _onItemTapped,
      ),
    );
  }
}
