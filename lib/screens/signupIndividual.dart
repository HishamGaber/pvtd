import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/providers/auth.dart';
import '/widgets/confirmpasswordWidget.dart';
import '/widgets/emailWidget.dart';
import '/widgets/passwordWidget.dart';
import '/widgets/textWidget.dart';

class SignupIndividual extends StatefulWidget {
  static const String routeName = '/signUpIndividual';
  @override
  _SignupIndividualState createState() => _SignupIndividualState();
}

class _SignupIndividualState extends State<SignupIndividual> {
  TextEditingController usernameController;
  TextEditingController emailController;
  TextEditingController passwordController;

  @override
  void initState() {
    super.initState();
    usernameController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context, listen: false);

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        reverse: true,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 40,
              ),
              Text(
                "انشاء حساب كفرد",
                style: const TextStyle(
                  color: const Color(0xff343433),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 24.0,
                ),
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextWidget(
                      text: 'اسم المستخدم',
                      hinttext: 'ادخل اسم المستخدم',
                      controller: usernameController,
                    ),
                    EmailWidget(
                      text: ' البريد الإلكتروني',
                      hinttext: 'ادخل  االبريد الإلكتروني',
                      controller: emailController,
                    ),
                    PasswordWidget(
                      text: 'كلمة المرور',
                      hinttext: 'ادخل كلمة المرور',
                      controller: passwordController,
                    ),
                    ConfirmpasswordWidget(
                      text: 'أعد إدخال كلمة المرور',
                      hinttext: 'ادخل كلمة المرور',
                      controller: passwordController,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 327,
                          height: 58,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7),
                            ),
                            color: const Color(0xff2d3693),
                          ),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                // If the form is valid, display a snackbar. In the real world,
                                // you'd often call a server or save the information in a database.
                                // ScaffoldMessenger.of(context).showSnackBar(
                                //     SnackBar(content: Text('Processing Data')));
                                auth
                                    .registerIndividual(
                                      emailController.text,
                                      passwordController.text,
                                      usernameController.text,
                                    )
                                    .then((success) => {
                                          if (success == false)
                                            {
                                              Fluttertoast.showToast(
                                                msg: auth.message,
                                                toastLength: Toast.LENGTH_LONG,
                                                gravity: ToastGravity.CENTER,
                                                timeInSecForIosWeb: 3,
                                                backgroundColor: Colors.grey,
                                                textColor: Colors.black,
                                                fontSize: 16.0,
                                              )
                                            }
                                          else
                                            {
                                              Navigator.of(context)
                                                  .pushNamed('/homepage'),
                                            }
                                        });
                              }
                            },
                            child: Text('انشاء حساب'),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Color.fromRGBO(45, 54, 147, 1)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
