import 'package:flutter/material.dart';

class ForgetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        reverse: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 40,
            ),
            Image(
              image: AssetImage('assets/images/bitmap.png'),
            ),
            SizedBox(
              height: 20,
            ),
            Text("أعد تعيين كلمة المرور",
                style: const TextStyle(
                    color: const Color(0xff343433),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Almarai",
                    fontStyle: FontStyle.normal,
                    fontSize: 24.0),
                textAlign: TextAlign.right),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: TextFormField(
                textAlign: TextAlign.right,
                decoration: InputDecoration(
                  hintText: ' البريد الإلكتروني',
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 327,
              height: 58,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  color: const Color(0xff2d3693)),
              child: ElevatedButton(
                onPressed: () {},
                child: Text('ارسل كلمة المرور'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color.fromRGBO(45, 54, 147, 1)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
