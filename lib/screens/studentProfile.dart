import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/userById.dart';
import 'package:pvtd_app/screens/studentEdit.dart';
import 'package:pvtd_app/screens/updatepassword.dart';

import 'home_screen.dart';

class StudentProfile extends StatefulWidget {
  @override
  _StudentProfileState createState() => _StudentProfileState();
}

bool is_init = true;
bool isLoading = true;

class _StudentProfileState extends State<StudentProfile> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (is_init) {
      final auth = Provider.of<Auth>(context, listen: false);
      // print('id   ${auth.id}');
      Provider.of<UserById>(context).getUserById(
        auth.id,
        auth.token,
      );

      is_init = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting(
      "ar",
    );
    final DateFormat formatter = DateFormat.yMMMd('ar_SA');
    final provide = Provider.of<UserById>(context);
    final auth = Provider.of<Auth>(context);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 20, 8, 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'أهلا',
                          style: SUBTITLE,
                        ),
                        Row(children: [
                          Text(
                            provide.userById?.user?.name == null
                                ? ''
                                : provide.userById?.user?.name,
                            style: TITLE_STYLE,
                          ),
                        ])
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'بياناتي',
                      style: SUBTITLE,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.edit,
                          color: Color.fromRGBO(45, 54, 147, 1),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed(StudentEdit.routeName);
                          },
                          child: Text(
                            'تعديل',
                            style: TextStyle(
                              color: Color.fromRGBO(45, 54, 147, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'الاسم',
                  leftText: provide.userById?.user?.name,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'تاريخ الميلاد',
                  leftText:
                      provide?.userById?.user?.studentProfile?.birthday != null
                          ? formatter.format(
                              provide?.userById?.user?.studentProfile?.birthday)
                          : '',
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'جهة الميلاد',
                  leftText:
                      provide.userById?.user?.studentProfile?.placeOfBirth,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'عنوان محل السكن',
                  leftText: provide.userById?.user?.address,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'المحافظة',
                  leftText: provide.userById?.user?.governorate?.arTitle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: ' القسم التابع له',
                  leftText: provide.userById?.user?.studentProfile?.department,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: ' رقم البطاقة الشخصية',
                  leftText: provide.userById?.user?.nationalId,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                    provide: provide,
                    rightText: 'مكتب السجل المدني',
                    leftText: provide
                        .userById?.user?.studentProfile?.civilRegistryOffice),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'المحافظة',
                  leftText: provide.userById?.user?.studentProfile
                      ?.civilRegistryGovernorateId,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'رقم الموبايل',
                  leftText: provide.userById?.user?.phone,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'رقم التليفون الأرضي',
                  leftText: provide.userById?.user?.landLine,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
                child: AccountCard(
                  provide: provide,
                  rightText: 'البريد الإلكتروني',
                  leftText: provide.userById?.user?.email,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: 60,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        Color.fromRGBO(40, 47, 115, 1),
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(UpdatePassword.routeName);
                    },
                    child: Text(
                      'تعديل كلمة المرور',
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    auth.logout().then((value) {
                      if (!value) {
                        Navigator.pushNamedAndRemoveUntil(
                            context,
                            HomeScreen.routeName,
                            (Route<dynamic> route) => false);
                      } else {
                        Fluttertoast.showToast(
                          msg: 'failed',
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 3,
                          backgroundColor: Colors.grey,
                          textColor: Colors.black,
                          fontSize: 16.0,
                        );
                      }
                    });
                  },
                  child: Text(
                    'تسجيل الخروج',
                    style: TextStyle(
                      color: Color.fromRGBO(40, 47, 115, 1),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class AccountCard extends StatelessWidget {
  final String leftText;
  final String rightText;

  const AccountCard({
    Key key,
    this.rightText,
    this.leftText,
    @required this.provide,
  }) : super(key: key);

  final UserById provide;

  @override
  Widget build(BuildContext context) {
    return Card(
        // elevation: 5,
        child: Padding(
      padding: const EdgeInsets.fromLTRB(8, 20, 8, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            rightText,
            style: SUBTITLE,
          ),
          Text(
            leftText == null ? '' : leftText,
            style: SUBTITLE,
          )
        ],
      ),
    ));
  }
}
