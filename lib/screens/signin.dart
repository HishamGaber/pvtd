import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pvtd_app/screens/signup.dart';

import 'home_screen.dart';

class Signin extends StatefulWidget {
  static const String routeName = '/signin';
  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool logged;
  bool isError = false;
  bool isButtonPressed = false;
  @override
  void initState() {
    super.initState();
    // emailController = TextEditingController();
    // passwordController = TextEditingController();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        reverse: true,
        child: Form(
          autovalidateMode: AutovalidateMode.always,
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 40,
              ),
              Image(
                image: AssetImage(
                  'assets/images/bitmap.png',
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'تسجيل الدخول',
                style: const TextStyle(
                  color: const Color(0xff343433),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 24.0,
                ),
                textAlign: TextAlign.right,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: TextFormField(
                  // onChanged: _formKey.currentState.reset(),
                  controller: emailController,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    hintText: 'البريد الإلكتروني',
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (!isButtonPressed) {
                      return null;
                    }
                    isError = true;
                    if (!(EmailValidator.validate(emailController.text))) {
                      return 'من فضلك ادخل بريد الكتروني صحيح';
                    }
                    isError = false;
                    return null;
                  },
                  // onFieldSubmitted: (value) {
                  //   if (!(EmailValidator.validate(emailController.text))) {
                  //     return 'من فضلك ادخل بريد الكتروني صحيح';
                  //   }
                  // },
                  onChanged: (value) {
                    isButtonPressed = false;
                    if (isError) {
                      _formKey.currentState.validate();
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: TextFormField(
                  controller: passwordController,
                  textAlign: TextAlign.right,
                  obscureText: true,
                  enableSuggestions: false,
                  autocorrect: false,
                  decoration: InputDecoration(
                    hintText: 'كلمة المرور',
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (!isButtonPressed) {
                      return null;
                    }
                    isError = true;
                    if (value == null || value.isEmpty) {
                      return 'من فضلك ادخل كلمة المرور';
                    } else if (value.length < 8) {
                      return 'كلمة المرور تتكون من ٨ عناصر على الاقل';
                    }
                    isError = false;
                    return null;
                  },
                  // onChanged: (value) {
                  //   isButtonPressed = false;
                  //   if (isError) {
                  //     _formKey.currentState.validate();
                  //   }
                  // },
                ),
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: Row(
                  children: [
                    Image(
                      image: AssetImage(
                        'assets/images/eye.png',
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('/forgetpassword');
                      },
                      child: Text(
                        "نسيت كلمة المرور",
                        style: const TextStyle(
                          color: const Color(0xffb7b7b8),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Almarai",
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 327,
                height: 58,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(7),
                  ),
                  color: const Color(0xff2d3693),
                ),
                child: ElevatedButton(
                  onPressed: () {
                    isButtonPressed = true;
                    if (_formKey.currentState.validate()) {
                      auth
                          .login(emailController.text, passwordController.text)
                          .then((success) => {
                                if (success)
                                  {
                                    Navigator.pushNamedAndRemoveUntil(
                                        context,
                                        HomeScreen.routeName,
                                        (Route<dynamic> route) => false)
                                  }
                                else
                                  {
                                    Fluttertoast.showToast(
                                      msg: auth.message,
                                      toastLength: Toast.LENGTH_LONG,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 3,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.black,
                                      fontSize: 16.0,
                                    )
                                  }
                              });
                    }
                  },
                  child: Text('تسجيل الدخول'),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                        Color.fromRGBO(45, 54, 147, 1)),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "ليس لديك حساب؟ ",
                    style: const TextStyle(
                      color: const Color(0xffb7b7b8),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0,
                    ),
                    textAlign: TextAlign.right,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(Signup.routeName);
                    },
                    child: Text(
                      "أنشئ حساب الأن",
                      style: const TextStyle(
                        color: const Color(0xff2d3693),
                        fontWeight: FontWeight.w700,
                        fontFamily: "Almarai",
                        fontStyle: FontStyle.normal,
                        fontSize: 14.0,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
