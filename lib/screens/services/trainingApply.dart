import 'package:cool_stepper/cool_stepper.dart';
import 'package:flutter/material.dart';
import 'package:pvtd_app/widgets/educationData.dart';
import 'package:pvtd_app/widgets/institutionData.dart';
import 'package:pvtd_app/widgets/personalData.dart';

class TrainingApply extends StatefulWidget {
  static const String routeName = '/trainingApply';
  TrainingApply({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TrainingApplyState createState() => _TrainingApplyState();
}

class _TrainingApplyState extends State<TrainingApply> {
  var currentStep = 0;
  final formKey = GlobalKey<FormState>();
  // final stepList;

  final stepList = [
    CoolStep(
      title: 'اختر المركز',
      content: InstitutionData(),
      subtitle: 'اختر المركز',
      validation: () {
        // if (!InstitutionDataState().formKey.currentState.validate()) {
        //   return 'kkkkk';
        // }
        // return null;
      },
      isHeaderEnabled: false,
    ),
    CoolStep(
      title: 'بيانات شخصية',
      content: PersonalData(),
      subtitle: '',
      validation: () {},
      isHeaderEnabled: false,
    ),
    CoolStep(
      title: 'بيانات تعليم ',
      content: EducationData(),
      isHeaderEnabled: false,
      validation: () {},
      subtitle: '',
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final send = Provider.of<SendRequest>(context);
    final stepper = CoolStepper(
      showErrorSnackbar: false,
      onCompleted: () {
        print('Steps completed!');
      },
      steps: stepList,
      config: CoolStepperConfig(
        backText: 'العودة',
        nextText: 'التالي',
        finalText: 'انتهاء',
      ),
    );
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        title: Text('اسم الدورة التدريبية'),
      ),
      // body: Container(
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Card(
          child: stepper,
        ),
      ),
    );
  }
}
