import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/skill.dart';
import 'package:pvtd_app/providers/tracks.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:pvtd_app/widgets/textWidget.dart';

import '../done.dart';

class SkillService extends StatefulWidget {
  static const String routeName = '/skillService';

  @override
  _SkillServiceState createState() => _SkillServiceState();
}

enum PreviousExperience { experienced, notExperienced }

class _SkillServiceState extends State<SkillService> {
  PreviousExperience _radioButtonValue = PreviousExperience.notExperienced;
  String _entityChosen;
  TextEditingController yearController = TextEditingController();
  TextEditingController qualificationController;
  String institutionId;
  String trackId;
  String dropdownValueInstitutions;
  String _token;
  String dropdownValueTrack;
  final _formKey = GlobalKey<FormState>();
  final radioKey = GlobalKey<FormState>();
  var institutionTraining;
  DateTime selectedDate;
  bool buttonPressed = false;
  TextEditingController experienceController;
  @override
  void initState() {
    institutionTraining?.tracksMenuInstitution = [];

    dropdownValueInstitutions = null;
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<Institution>(context, listen: false)
            .fetchAndSetInstitutions());
    selectedDate = DateTime(2020);
    super.initState();
    qualificationController = TextEditingController();
    experienceController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    // TextEditingController graduationController;

    // final send = Provider.of<SendRequest>(context);
    final institution = Provider.of<Institution>(context);
    final institutionTraining = Provider.of<InstitutionTraining>(context);
    final auth = Provider.of<Auth>(context);

    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('خدمة قياس مستوى المهارة'),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(
                            'اختر المهارة التي ترغب بحضور الاختبار الخاص بها',
                            style: TITLE_STYLE,
                          ),
                          DefaultInstitutionDropDown(
                              label: 'المركز',
                              onChanged: (Institution institution) {
                                setState(() {
                                  dropdownValueInstitutions = institution.id;
                                  institutionTraining
                                      .fetchAndSetTracksByInstitution(
                                          institution.id);
                                });
                              }),
                          SizedBox(
                            height: 10,
                          ),
                          DefaultTracksDropDown(
                              label: 'المهارة',
                              onChanged: (InstitutionTraining track) {
                                dropdownValueTrack = track.id;
                              }),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'بيانات شخصية',
                                style: TITLE_STYLE,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          DefaultEntityDropDown(
                              label: 'الجهة التابع لها(شركة/أفراد)',
                              onChanged: (String value) {
                                _entityChosen = value;
                              }),
                          SizedBox(
                            height: 10,
                          ),
                          TextWidget(
                            text: 'المؤهل الدراسي',
                            hinttext: 'المؤهل الدراسي',
                            controller: qualificationController,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'سنة التخرج',
                                style: SUBTITLE,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            controller: yearController,
                            textAlign: TextAlign.right,
                            decoration: InputDecoration(
                              hintText: 'سنة التخرج',
                              border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(10.0),
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'من فضلك ادخل موضوع الاستشارة';
                              }
                              if (value.length != 4 ||
                                  int.parse(value) > 2021 ||
                                  int.parse(value) < 2000) {
                                return 'من فضلك ادخل سنة صحيحة';
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'خبرات سابقة',
                                style: SUBTITLE,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Column(
                            children: [
                              ListTile(
                                title: const Text('يوجد'),
                                leading: Radio<PreviousExperience>(
                                  value: PreviousExperience.experienced,
                                  groupValue: _radioButtonValue,
                                  onChanged: (PreviousExperience value) {
                                    setState(() {
                                      _radioButtonValue = value;
                                    });
                                  },
                                ),
                              ),
                              ListTile(
                                title: const Text('لا يوجد'),
                                leading: Radio<PreviousExperience>(
                                  value: PreviousExperience.notExperienced,
                                  groupValue: _radioButtonValue,
                                  onChanged: (PreviousExperience value) {
                                    setState(() {
                                      _radioButtonValue = value;
                                    });
                                  },
                                ),
                              ),
                              _radioButtonValue ==
                                      PreviousExperience.experienced
                                  ? TextFormField(
                                      maxLines: 8,
                                      controller: experienceController,
                                      textAlign: TextAlign.right,
                                      decoration: InputDecoration(
                                        hintText: 'اكتب خبراتك السابقة',
                                        border: OutlineInputBorder(
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                      validator: (value) {
                                        if ((value == null || value.isEmpty) &&
                                            _radioButtonValue ==
                                                PreviousExperience
                                                    .experienced) {
                                          return 'من فضلك ادخل المطلوب';
                                        } else {
                                          return null;
                                        }
                                      },
                                    )
                                  : Container(),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  ),
                ),
                onPressed: () async {
                  setState(() {
                    buttonPressed = true;
                  });

                  if (_formKey.currentState.validate()) {
                    SendRequest.sendSkill(
                        token: auth.token,
                        skill: Skill(
                          institutionId: dropdownValueInstitutions,
                          institutionTrackId: dropdownValueTrack,
                          entity: _entityChosen == 'شركة  '
                              ? 'COMPANY'
                              : 'INDIVIDUAL',
                          educationalQualification: 'nlvjnl',
                          // qualificationController?.text,
                          graduationYear: yearController?.text,
                          previousExperience: experienceController?.text,
                          hasExperience: PreviousExperience.experienced != null
                              ? true
                              : false,
                        )).then((value) {
                      if (value?.message == null) {
                        Navigator.pushNamed(context, Done.routeName);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(value?.message),
                          ),
                        );
                      }
                    });
                  }
                },
                child: Text('قدم الطلب'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
