import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/api_urls.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/maintenance.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import '../done.dart';
import '../fail.dart';

class MaintenanceService extends StatefulWidget {
  static const String routeName = '/maintenance';
  @override
  _MaintenanceServiceState createState() => _MaintenanceServiceState();
}

enum PaymentMethod { cash, bank }

class _MaintenanceServiceState extends State<MaintenanceService> {
  // String dropdownValue = 'مركز الأميرية التجريبي';
  List<String> institutionsMenu = [];
  TextEditingController _addressController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  String dropdownValueInstitutions;
  bool is_init = true;
  PaymentMethod _character = PaymentMethod.cash;
  String institutionId;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<Institution>(context, listen: false)
            .fetchAndSetInstitutions());
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    final auth = Provider.of<Auth>(context);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('خدمة الصيانة'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'اختر المركز الذي ترغب بالصيانة فيه',
                            style: TITLE_STYLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      DefaultInstitutionDropDown(
                          label: 'المركز',
                          onChanged: (Institution value) {
                            dropdownValueInstitutions = value.id;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'بيانات شخصية',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 24.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'العنوان',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        controller: _addressController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'ادخل العنوان',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'من فضلك ادخل العنوان';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'توصيف خدمة الصيانة',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        maxLines: 10,
                        controller: _descriptionController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'توصيف الخدمة',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'من فضلك ادخل توصيف الخدمة';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'طريقة الدفع',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: [
                          ListTile(
                            title: const Text('تحويل نقدي'),
                            leading: Radio<PaymentMethod>(
                              value: PaymentMethod.cash,
                              groupValue: _character,
                              onChanged: (PaymentMethod value) {
                                setState(() {
                                  _character = value;
                                });
                              },
                            ),
                          ),
                          ListTile(
                            title: const Text('تحويل بنكي'),
                            leading: Radio<PaymentMethod>(
                              value: PaymentMethod.bank,
                              groupValue: _character,
                              onChanged: (PaymentMethod value) {
                                setState(() {
                                  _character = value;
                                });
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  ),
                ),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    SendRequest.sendMaintenance(
                      maintenance: Maintenance(
                        address: _addressController.text,
                        description: _descriptionController.text,
                        institutionId: dropdownValueInstitutions,
                        paymentMethod:
                            PaymentMethod.bank == null ? 'CASH' : 'BANK',
                      ),
                      token: auth.token,
                    ).then((value) {
                      if (value?.message == null) {
                        Navigator.pushNamed(context, Done.routeName);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(value?.message),
                          ),
                        );
                      }
                    });
                  }
                },
                child: Text('قدم الطلب'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
