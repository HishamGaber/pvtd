import 'package:cool_stepper/cool_stepper.dart';
import 'package:flutter/material.dart';
import 'package:pvtd_app/widgets/contact.dart';
import 'package:pvtd_app/widgets/programs.dart';
import 'package:pvtd_app/widgets/qualification.dart';

class TrainingService extends StatefulWidget {
  static const String routeName = '/trainingService';
  TrainingService({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TrainingServiceState createState() => _TrainingServiceState();
}

class _TrainingServiceState extends State<TrainingService> {
  var currentStep = 0;
  final formKey = GlobalKey<FormState>();
  // final stepList;

  final stepList = [
    CoolStep(
      title: 'المؤهلات العلمية',
      content: Qualification(),
      subtitle: '',
      validation: () {},
      isHeaderEnabled: false,
    ),
    CoolStep(
      title: 'بيانات العمل',
      content: Contact(),
      subtitle: '',
      validation: () {},
      isHeaderEnabled: false,
    ),
    CoolStep(
      title: 'البرامج ',
      content: Programs(),
      subtitle: '',
      validation: () {},
      isHeaderEnabled: false,
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final stepper = CoolStepper(
      showErrorSnackbar: false,
      onCompleted: () {
        print('Steps completed!');
      },
      steps: stepList,
      config: CoolStepperConfig(
        backText: 'العودة',
        nextText: 'التالي',
        finalText: 'انتهاء',
      ),
    );
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        title: Text('اسم الدورة التدريبية'),
      ),
      // body: Container(
      body: Padding(
        padding: const EdgeInsets.all(11.0),
        child: Card(child: stepper),
      ),
    );
  }
}
