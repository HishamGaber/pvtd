import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import '/common/styles.dart';
import '/providers/Certificate.dart';
import '/providers/auth.dart';
import '/providers/institutions.dart';
import '/providers/sendRequest.dart';
import '/providers/tracks.dart';
import '../done.dart';

class CertificateService extends StatefulWidget {
  static const String routeName = '/certificateService';
  @override
  _CertificateServiceState createState() => _CertificateServiceState();
}

class _CertificateServiceState extends State<CertificateService> {
  final TextEditingController _graduationController = TextEditingController();
  final TextEditingController _toController = TextEditingController();
  bool _transcript = false;
  bool _successStatement = false;
  bool _certificateOriginal = false;
  bool _certificateExtract = false;
  bool _caseStatement = false;
  bool _skill = false;
  String _token;
  bool is_init = true;
  var institutionTraining;

  String institutionId;
  String trackId;

  // List<String> tracksMenu = [];

  String dropdownValueTrack;
  String dropdownValueInstitutions;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // institutionTraining?.tracksMenuInstitution = [];

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<Institution>(context, listen: false)
            .fetchAndSetInstitutions());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    institutionTraining = Provider.of<InstitutionTraining>(context);
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    final auth = Provider.of<Auth>(context);
    // institutionTraining.tracksMenuInstitution.clear();
    final institutionTraining = Provider.of<InstitutionTraining>(context);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('خدمات الشهادات'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'استخرج الشهادات التالية',
                            style: TITLE_STYLE,
                          )
                        ],
                      ),
                    ),
                    CheckboxListTile(
                      title: Text(
                        'استخرج الشهادات التالية',
                      ),
                      value: _transcript,
                      onChanged: (newValue) {
                        setState(() {
                          _transcript = !_transcript;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                    CheckboxListTile(
                      title: Text("بيان نجاح"),
                      value: _successStatement,
                      onChanged: (newValue) {
                        setState(() {
                          _successStatement = !_successStatement;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                    CheckboxListTile(
                      title: Text("شهادة تخرج اصلية"),
                      value: _certificateOriginal,
                      onChanged: (newValue) {
                        setState(() {
                          _certificateOriginal = !_certificateOriginal;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                    CheckboxListTile(
                      title: Text("شهادة تخرج مستخرج "),
                      value: _certificateExtract,
                      onChanged: (newValue) {
                        setState(() {
                          _certificateExtract = !_certificateExtract;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                    CheckboxListTile(
                      title: Text("بيان حالة"),
                      value: _caseStatement,
                      onChanged: (newValue) {
                        setState(() {
                          _caseStatement = !_caseStatement;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                    CheckboxListTile(
                      title: Text("المهارة"),
                      value: _skill,
                      onChanged: (newValue) {
                        setState(() {
                          _skill = !_skill;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'بيانات شخصية',
                            style: TITLE_STYLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      DefaultInstitutionDropDown(
                        label: 'المركز الذي درست فيه',
                        onChanged: (Institution value) {
                          dropdownValueInstitutions = value.id;
                          institutionTraining.fetchAndSetTracksByInstitution(
                              dropdownValueInstitutions);
                        },
                      ),
                      DefaultTracksDropDown(
                          label: 'التخصص',
                          onChanged: (InstitutionTraining value) {
                            dropdownValueTrack = value.eductionTrackId;
                          }),
                      SizedBox(
                        height: 10,
                      ),
                      YearWidget(
                        text: 'سنه التخرج',
                        hinttext: 'سنه التخرج',
                        controller: _graduationController,
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.start,
                      //   children: [
                      //     Text(
                      //       'سنة التخرج',
                      //       style: const TextStyle(
                      //         color: const Color(0xff26262b),
                      //         fontWeight: FontWeight.w700,
                      //         fontFamily: "Almarai",
                      //         fontStyle: FontStyle.normal,
                      //         fontSize: 16.0,
                      //       ),
                      //       textAlign: TextAlign.start,
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      // TextFormField(
                      //   controller: _graduationController,
                      //   textAlign: TextAlign.right,
                      //   decoration: InputDecoration(
                      //     hintText: 'سنه التخرج',
                      //     border: OutlineInputBorder(
                      //       borderRadius: const BorderRadius.all(
                      //         const Radius.circular(10.0),
                      //       ),
                      //     ),
                      //   ),
                      //   validator: (value) {
                      //     if (value == null || value.isEmpty) {
                      //       return 'من فضلك ادخل سنه التخرج';
                      //     }
                      //     return null;
                      //   },
                      // ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'وذلك لتقديمها إلى ',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        controller: _toController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'وذلك لتقديمها إلى ',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'من فضلك ادخل الجهة المقدم اليها';
                          }
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  ),
                ),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    SendRequest.sendCertificate(
                      token: auth.token,
                      certificate: Certificate(
                        certificateGraduationExtract: _certificateExtract,
                        eductionTrackId: dropdownValueTrack,
                        graduationYear: _graduationController.text,
                        institutionId: dropdownValueInstitutions,
                        originalGraduationCertificate: _certificateOriginal,
                        skill: _skill,
                        statementCase: _caseStatement,
                        successStatement: _successStatement,
                        targetPlace: _toController.text,
                        transcripts: _transcript,
                      ),
                    ).then((value) {
                      if (value?.message == null) {
                        Navigator.pushNamed(context, Done.routeName);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(value?.message),
                          ),
                        );
                      }
                    });
                  }
                },
                child: Text('قدم الطلب'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
