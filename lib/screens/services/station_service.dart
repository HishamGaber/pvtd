import 'dart:io';

import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/api_urls.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/imageResponse.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/station.dart';
import 'package:pvtd_app/providers/trackType.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http_parser/http_parser.dart';

import '../done.dart';
import '../fail.dart';

class StationService extends StatefulWidget {
  static const String routeName = '/stationService';
  final Function onSelectImage;

  const StationService({this.onSelectImage});
  @override
  _StationServiceState createState() => _StationServiceState();
}

class _StationServiceState extends State<StationService> {
  List<int> selectedItems = [];
  bool image1Uploaded = false;
  bool image2Uploaded = false;
  bool image3Uploaded = false;
  bool image4Uploaded = false;
  File _storedImage;

  final List<DropdownMenuItem> items = [];

  ImageResponse imageResponse;
  String name;
  String industrialRegistry;
  String commercialRegister;
  String taxCard;
  String titlingContract;
  String token;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<TrackType>(this.context, listen: false).fetchTrackTypes());

    // WidgetsBinding.instance.addPostFrameCallback((_) =>
    //     Provider.of<InstitutionTraining>(context, listen: false)
    //         .fetchAndSetTracks());
  }

  Future<void> _takePicture() async {
    final picker = ImagePicker();
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
    var imageFile = File(pickedFile?.path);

    // setState(() {
    //   _storedImage = imageFile;
    // });
    final appDir = await syspaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    // setState(() {
    //   name = fileName;
    // });

    upload(
      File(imageFile.path),
    );
  }

  Future<void> upload(File imageFile) async {
    // open a bytestream
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    // get file length
    var length = await imageFile.length();

    // string to uri
    var uri = Uri.parse(MAIN_URL + "upload/image");

    var request = new http.MultipartRequest("POST", uri);
    request.files.add(http.MultipartFile(
      'image',
      stream,
      length,
      filename: basename(imageFile.path),
      contentType: MediaType('image', 'jpg'),
    ));
    request.send().then((response) async {
      final respStr = await response.stream.bytesToString();
      var jj = json.decode(respStr);
      imageResponse = ImageResponse.fromJson(jj);
      print(imageResponse.fileUrl);
      if (response.statusCode == 200) print("Uploaded!");
    });
  }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context);
    final tracksProvider = Provider.of<TrackType>(this.context, listen: false);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text(' معاينة لإنشاء محطة تدريبية'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'التخصصات التي ترغب بها',
                          style: SUBTITLE,
                        ),
                      ],
                    ),
                    Consumer<TrackType>(
                      builder: (BuildContext context, model, Widget child) {
                        return SearchableDropdown.multiple(
                          items: model?.trackTypes != null
                              ? model?.trackTypes?.map((TrackType value) {
                                  return DropdownMenuItem(
                                    value: value.id,
                                    child: Text(value.arTitle),
                                  );
                                })?.toList()
                              : [],
                          selectedItems: selectedItems,
                          hint: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Text("اختر التخصصات"),
                          ),
                          // searchHint: "اختر التخصصات",
                          onChanged: (value) {
                            setState(() {
                              selectedItems = value;
                            });
                          },
                          closeButton: (selectedItems) {
                            print(selectedItems);
                            return (selectedItems.isNotEmpty
                                ? "Save ${selectedItems.length == 0 ? '"' + items[selectedItems.first].value.toString() + '"' : '(' + selectedItems.length.toString() + ')'}"
                                : "Save without selection");
                          },
                          isExpanded: true,
                        );
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'أوراق مطلوبه',
                          style: TITLE_STYLE,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'السجل الصناعي',
                          style: SUBTITLE,
                        ),
                      ],
                    ),
                    OutlinedButton(
                      onPressed: () async {
                        await _takePicture();

                        setState(() {
                          image1Uploaded = true;
                          industrialRegistry = imageResponse?.fileName;
                        });
                      },
                      child: Column(
                        children: [
                          Image(
                            image:
                                AssetImage('assets/images/cloudComputing.png'),
                          ),
                          Text('قم برفع الملف'),
                        ],
                      ),
                    ),
                    image1Uploaded
                        ? SafeArea(
                            child: Row(
                              children: [
                                Expanded(child: Text('$industrialRegistry')),
                              ],
                            ),
                          )
                        : Text(''),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'السجل التجاري',
                          style: SUBTITLE,
                        ),
                      ],
                    ),
                    OutlinedButton(
                      onPressed: () async {
                        await _takePicture();
                        setState(() {
                          image2Uploaded = true;
                          commercialRegister = imageResponse?.fileName;
                        });
                      },
                      child: Column(
                        children: [
                          Image(
                            image:
                                AssetImage('assets/images/cloudComputing.png'),
                          ),
                          Text('قم برفع الملف'),
                        ],
                      ),
                    ),
                    image2Uploaded
                        ? Row(
                            children: [
                              Expanded(child: Text('$commercialRegister')),
                            ],
                          )
                        : Text(''),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'البطاقة الضريبية',
                          style: SUBTITLE,
                        ),
                      ],
                    ),
                    OutlinedButton(
                      onPressed: () async {
                        await _takePicture();
                        setState(() {
                          taxCard = imageResponse?.fileName;
                          image3Uploaded = true;
                        });
                      },
                      child: Column(
                        children: [
                          Image(
                            image:
                                AssetImage('assets/images/cloudComputing.png'),
                          ),
                          Text('قم برفع الملف'),
                        ],
                      ),
                    ),
                    image3Uploaded
                        ? Row(
                            children: [
                              Expanded(child: Text('$taxCard')),
                            ],
                          )
                        : Text(''),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'عقد تمليك او ايجار موثق',
                          style: SUBTITLE,
                        ),
                      ],
                    ),
                    OutlinedButton(
                      onPressed: () async {
                        await _takePicture();
                        setState(() {
                          titlingContract = imageResponse?.fileName;
                          image4Uploaded = true;
                        });
                      },
                      child: Column(
                        children: [
                          Image(
                            image:
                                AssetImage('assets/images/cloudComputing.png'),
                          ),
                          Text('قم برفع الملف'),
                        ],
                      ),
                    ),
                    image4Uploaded
                        ? Row(
                            children: [
                              Expanded(child: Text('$titlingContract')),
                            ],
                          )
                        : Text(''),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  ),
                ),
                onPressed: () async {
                  List<String> TrackTypeslist = [];
                  for (int i = 0; i < selectedItems.length; i++) {
                    TrackTypeslist.add(tracksProvider.trackTypes[i].id);
                  }
                  SendRequest.sendStation(
                      token: auth.token,
                      station: Station(
                        industrialRegistry: industrialRegistry,
                        commercialRegister: commercialRegister,
                        taxCard: taxCard,
                        titlingContract: titlingContract,
                        trackTypes: TrackTypeslist,
                      )).then((value) {
                    print(value.toString());
                    if (value?.message == null) {
                      Navigator.pushNamed(context, Done.routeName);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text(value?.message)));
                    }
                  });
                },
                child: Text('قدم الطلب'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
