import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/consolation.dart';
import 'package:pvtd_app/screens/fail.dart';
import '../../providers/sendRequest.dart';
import '../done.dart';
import '/providers/auth.dart';

class ConsolationService extends StatelessWidget {
  static const routeName = '/consolationService';

  final TextEditingController topicController = TextEditingController();
  final TextEditingController targetController = TextEditingController();
  final TextEditingController otherController = TextEditingController();
  String token;

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Center(child: Text('خدمة الاستشارة')),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'بيانات شخصية',
                        style: TITLE_STYLE,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'موضوع الاستشارة',
                        style: SUBTITLE,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: topicController,
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      hintText: 'موضوع الاستشارة',
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'من فضلك ادخل موضوع الاستشارة';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('الهدف من الاستشارة', style: SUBTITLE),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: targetController,
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      hintText: 'الهدف من الاستشارة',
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'من فضلك ادخل الهدف من الاستشارة';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('اي بيانات أخرى', style: SUBTITLE),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: otherController,
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      hintText: 'ادخل اي بيانات ترغب في اضافتها',
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'من فضلك ادخل البيانات الاخرى';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 60,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          Color.fromRGBO(45, 54, 147, 1),
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          token = auth.token;
                          print('token: $token');
                          SendRequest.sendConsolation(
                            consolation: Consolation(
                              topic: topicController?.text,
                              target: targetController?.text,
                              otherData: otherController?.text,
                            ),
                            token: token,
                          ).then((value) {
                            if (value?.message == null) {
                              Navigator.pushNamed(context, Done.routeName);
                            } else {
                              Navigator.pushNamed(context, Fail.routeName);
                            }
                          });
                        }
                      },
                      child: Text('قدم الطلب'),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
