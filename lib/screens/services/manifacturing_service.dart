import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/manifacture.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import '../done.dart';
import '../fail.dart';

class ManufacturingService extends StatefulWidget {
  static const String routeName = '/manifacturingService';
  @override
  _ManufacturingServiceState createState() => _ManufacturingServiceState();
}

enum PaymentMethod { cash, bank }

class _ManufacturingServiceState extends State<ManufacturingService> {
  // String dropdownValue = 'مركز الأميرية التجريبي';
  List<String> institutionsMenu = [];
  TextEditingController _serviceController = TextEditingController();
  TextEditingController _entityNameController = TextEditingController();
  String _entityChosen;

  String dropdownValueInstitutions;
  bool is_init = true;
  PaymentMethod _character;
  String institutionId;
  bool _check1 = false;
  bool _check2 = false;
  String token;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<Institution>(context, listen: false)
            .fetchAndSetInstitutions());
  }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context);
    final institution = Provider.of<Institution>(context);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('خدمة التصنيع لدى الغير'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'بيانات الخدمة',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 24.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      DefaultInstitutionDropDown(
                          label: 'المركز',
                          onChanged: (Institution value) {
                            dropdownValueInstitutions = value.id;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      DefaultEntityDropDown(
                          label: 'الجهة',
                          onChanged: (String value) {
                            _entityChosen = value;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'ادخل اسم الجهة ان وجد',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        controller: _entityNameController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'ادخل اسم الجهة',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'توصيف الخدمة',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        maxLines: 10,
                        controller: _serviceController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'توصيف الخدمة',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'من فضلك ادخل توصيف الخدمة';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CheckboxListTile(
                        title: Text('سأقوم بتوفير الخامات'),
                        value: _check1,
                        onChanged: (value) {
                          setState(() {
                            _check1 = !_check1;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                      CheckboxListTile(
                        title:
                            Text('سأقوم بتوفير رسومات كاملة التفاصيل والأبعاد'),
                        value: _check2,
                        onChanged: (value) {
                          setState(() {
                            _check2 = !_check2;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'طريقة الدفع',
                            style: const TextStyle(
                              color: const Color(0xff26262b),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Almarai",
                              fontStyle: FontStyle.normal,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: [
                          ListTile(
                            title: const Text('تحويل نقدي'),
                            leading: Radio<PaymentMethod>(
                              value: PaymentMethod.cash,
                              groupValue: _character,
                              onChanged: (PaymentMethod value) {
                                setState(() {
                                  _character = value;
                                });
                              },
                            ),
                          ),
                          ListTile(
                            title: const Text('تحويل بنكي'),
                            leading: Radio<PaymentMethod>(
                              value: PaymentMethod.bank,
                              groupValue: _character,
                              onChanged: (PaymentMethod value) {
                                setState(() {
                                  _character = value;
                                });
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  ),
                ),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    SendRequest.sendManifacturing(
                      token: auth.token,
                      manifacture: Manifacture(
                        institutionId: dropdownValueInstitutions,
                        entity: _entityChosen == '  شركة'
                            ? 'COMPANY'
                            : 'INDIVIDUAL',
                        entityName: _entityNameController.text,
                        description: _serviceController.text,
                        providingDimensionalDrawings: _check1,
                        providingRawMaterials: _check2,
                        paymentMethod:
                            PaymentMethod.bank != null ? 'BANK' : 'CASH',
                      ),
                    ).then((value) {
                      if (value?.message == null) {
                        Navigator.pushNamed(context, Done.routeName);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(value?.message),
                          ),
                        );
                      }
                    });
                  }
                },
                child: Text('قدم الطلب'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
