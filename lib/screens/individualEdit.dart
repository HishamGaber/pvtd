import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/network/updateUser.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/governorate.dart' as gov;
import 'package:pvtd_app/providers/individual.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/userById.dart';
import 'package:pvtd_app/screens/fail.dart';
import 'package:pvtd_app/widgets/emailWidget.dart';
import 'package:pvtd_app/widgets/mobileWidget.dart';
import 'package:pvtd_app/widgets/nationalidWidget.dart';
import 'package:pvtd_app/widgets/textWidget.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class IndividualEdit extends StatefulWidget {
  static const String routeName = '/individualEdit';
  @override
  _IndividualEditState createState() => _IndividualEditState();
}

class _IndividualEditState extends State<IndividualEdit> {
  TextEditingController fullNameController;
  TextEditingController nationalIdController;
  TextEditingController emailController;
  TextEditingController addressController;

  TextEditingController landLineController;
  TextEditingController mobileController;
  final _formKey = GlobalKey<FormState>();
  String governorateDropDown;
  String governorateId;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Provider.of<gov.Governorate>(context, listen: false)
          .fetchAndSetGovernorates(),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final provide = Provider.of<UserById>(context);
    fullNameController = TextEditingController(
      text: provide.userById?.user?.name,
    );
    nationalIdController = TextEditingController(
      text: provide.userById?.user?.nationalId,
    );

    addressController = TextEditingController(
      text: provide.userById?.user?.address,
    );

    mobileController = TextEditingController(
      text: provide.userById?.user?.phone,
    );
    landLineController = TextEditingController(
      text: provide.userById?.user?.landLine,
    );
    emailController = TextEditingController(
      text: provide.userById?.user?.email,
    );
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    final auth = Provider.of<Auth>(context);
    final governorate = Provider.of<gov.Governorate>(context);
    final provide = Provider.of<UserById>(context);
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('تعديل بياناتي'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              elevation: 30,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'بيانات شخصية',
                            style: TITLE_STYLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'الاسم بالكامل',
                        hinttext: 'الاسم بالكامل',
                        controller: fullNameController,
                      ),
                      NationalidWidget(
                        text: 'الرقم القومي',
                        hinttext: 'الرقم القومي',
                        controller: nationalIdController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'المحافظة',
                            style: TITLE_STYLE,
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Consumer<gov.Governorate>(
                            builder:
                                (BuildContext context, model, Widget child) {
                              return Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: SearchableDropdown.single(
                                    value: governorateDropDown,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'من فضلك ادخل المحافظة';
                                      }
                                      return null;
                                    },
                                    searchFn: institution.searchfn,
                                    items: model.governorates
                                        .map((gov.Governorate value) {
                                      return DropdownMenuItem(
                                        value: value.id,
                                        child: Text(value.arTitle),
                                      );
                                    }).toList(),
                                    // value: 'dropdownValueInstitutions',
                                    hint: "اختر المحافظة",
                                    searchHint: null,
                                    underline: Container(),
                                    onChanged: (value) {
                                      setState(() {
                                        governorateDropDown = value;
                                      });
                                    },
                                    isExpanded: true,
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'العنوان',
                        hinttext: 'أدخل العنوان',
                        controller: addressController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MobileWidget(
                        text: 'رقم الموبايل',
                        hinttext: 'رقم الموبايل',
                        controller: mobileController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'رقم التليفون الارضي',
                            style: TITLE_STYLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        // validator: (value) {
                        //   if (value == null || value.isEmpty) {
                        //     return 'من فضلك ادخل رقم';
                        //   }
                        //   return null;
                        // },
                        keyboardType: TextInputType.number,
                        controller: landLineController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'رقم التليفون الارضي',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      EmailWidget(
                        controller: emailController,
                        text: 'البريد الإلكتروني',
                        hinttext: 'البريد الإلكتروني',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(40, 47, 115, 1),
                    ),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      SendRequest.updateIndividual(
                        token: auth.token,
                        indidvidual: Individual(
                          email: emailController.text,
                          username: fullNameController.text,
                          nationalId: nationalIdController.text,
                          governorateId: governorateDropDown,
                          address: addressController.text,
                          phone: mobileController.text,
                          landline: landLineController.text,
                        ),
                      ).then((value) {
                        print('value $value');
                        if (value?.message == null) {
                          print('${auth.id}  ${auth.token}');
                          provide.getUserById(auth.id, auth.token);
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Success'),
                            ),
                          );
                        } else {
                          Navigator.pushNamed(context, Fail.routeName);
                        }
                      });
                    }
                  },
                  child: Text('حفظ'),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
