import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';

class NewsDetails extends StatelessWidget {
  final String text;
  final String title;
  final String type;
  final String image;
  NewsDetails({
    this.text,
    this.title,
    this.type,
    this.image,
  }) : super();
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context);

    return Scaffold(
      // backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Color.fromRGBO(45, 54, 147, 1),
        title: Text(this.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Image.network(image),
            Text(
              this.title,
              style: TITLE_STYLE,
            ),
            Html(
              data: this.text,
              style: {
                "body": Style(
                  fontSize: FontSize(16.0),
                  fontWeight: FontWeight.normal,
                  lineHeight: LineHeight(1.8),
                ),
              },
            ),
          ],
        ),
      ),
    );
  }
}
