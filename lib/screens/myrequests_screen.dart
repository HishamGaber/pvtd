import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/myRequestsResponse.dart';

class MyRequestsScreen extends StatefulWidget {
  static const String routeName = '/myRequestsScreen';
  @override
  _MyRequestsScreenState createState() => _MyRequestsScreenState();
}

class _MyRequestsScreenState extends State<MyRequestsScreen> {
  var is_init = true;
  bool isLoading = true;
  @override
  void didChangeDependencies() {
    if (is_init) {
      final auth = Provider.of<Auth>(context);
      String token = auth.token;
      Provider.of<MyRequestsResponse>(context)
          .fetchAndSetMyRequests(token)
          .then((_) {
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      });
    }
    is_init = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final myRequests = Provider.of<MyRequestsResponse>(context);

    List<RequestCard> requestsCards() {
      List<RequestCard> list = [];
      //i<5, pass your dynamic limit as per your requirment

      if (myRequests.myRequestsResponse?.user?.requestCertificates != null) {
        print(myRequests.myRequestsResponse?.user?.requestCertificates?.length);
        for (int i = 0;
            i <
                myRequests
                    .myRequestsResponse?.user?.requestCertificates?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestCertificates[i].state,
              requestName: 'طلب شهادة',
            ),
          );
        }
      }

      if (myRequests.myRequestsResponse?.user?.requestApplyingToTrainings !=
          null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i <
                myRequests.myRequestsResponse?.user?.requestApplyingToTrainings
                    ?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests.myRequestsResponse?.user
                  ?.requestApplyingToTrainings[i].state,
              requestName: 'طلب الالتحاق بمراكز التدريب',
            ),
          );
        }
      }
      if (myRequests.myRequestsResponse?.user?.requestConsolations != null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i <
                myRequests
                    .myRequestsResponse?.user?.requestConsolations?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestConsolations[i].state,
              requestName: 'طلب استشارة',
            ),
          );
        }
      }
      if (myRequests.myRequestsResponse?.user?.requestMaintenances != null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i <
                myRequests
                    .myRequestsResponse?.user?.requestMaintenances?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestMaintenances[i].state,
              requestName: 'طلب صيانة',
            ),
          );
        }
      }
      if (myRequests.myRequestsResponse?.user?.requestMeasurementSkills !=
          null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i <
                myRequests
                    .myRequestsResponse?.user?.requestMeasurementSkills?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestMeasurementSkills[i].state,
              requestName: 'طلب قياس مستوى المهارة',
            ),
          );
        }
      }
      if (myRequests.myRequestsResponse?.user?.requestManufacturings != null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i <
                myRequests
                    .myRequestsResponse?.user?.requestManufacturings?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestManufacturings[i].state,
              requestName: 'طلب تصنيع',
            ),
          );
        }
      }
      if (myRequests.myRequestsResponse?.user?.requestSetupStations != null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i <
                myRequests
                    .myRequestsResponse?.user?.requestSetupStations?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestSetupStations[i].state,
              requestName: 'طلب انشاء محطة تدريبية',
            ),
          );
        }
      }

      if (myRequests.myRequestsResponse?.user?.requestTrainings != null) {
        // print(myRequests.myRequestsResponse.user.requestCertificates.length);
        for (int i = 0;
            i < myRequests.myRequestsResponse?.user?.requestTrainings?.length;
            i++) {
          list.add(
            RequestCard(
              status: myRequests
                  .myRequestsResponse?.user?.requestTrainings[i].state,
              requestName: 'طلب دورة تدريبية قصيرة',
            ),
          );
        }
      }

      setState(() {
        isLoading = false;
      });
      return list; // all widget added now retrun the list here
    }

    return Scaffold(
      backgroundColor: Colors.indigo[50],
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView(
              children: [
                Column(
                  children: requestsCards(),
                ),
              ],
            ),
    );
  }
}

class RequestCard extends StatelessWidget {
  final String status;
  final String requestName;
  const RequestCard({Key key, this.status, this.requestName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 1, 8, 1),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    requestName,
                    style: TITLE_STYLE,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text(
                    'حالة الطلب',
                    style: GREYSTYLE,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  if (status == 'PENDING')
                    Text(
                      'قيد المراجعة',
                      style: GREYSTYLE,
                    ),
                  if (status == 'ACCEPTED')
                    Text(
                      'تم قبول الطلب',
                      style: GREENSTYLE,
                    ),
                  if (status == 'REJECTED')
                    Text(
                      'تم رفض الطلب',
                      style: REDSTYLE,
                    ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
