import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/governorate.dart' as gov;
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/student.dart';
import 'package:pvtd_app/providers/userById.dart';
import 'package:pvtd_app/widgets/dateWidget.dart';
import 'package:pvtd_app/widgets/emailWidget.dart';
import 'package:pvtd_app/widgets/mobileWidget.dart';
import 'package:pvtd_app/widgets/nationalidWidget.dart';
import 'package:pvtd_app/widgets/textWidget.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'fail.dart';

class StudentEdit extends StatefulWidget {
  static const String routeName = '/studentEdit';
  @override
  _StudentEditState createState() => _StudentEditState();
}

class _StudentEditState extends State<StudentEdit> {
  TextEditingController fullNameController;
  TextEditingController nationalIdController;
  TextEditingController emailController;
  TextEditingController addressController;
  TextEditingController birthPlaceController;
  TextEditingController landLineController;
  TextEditingController mobileController;
  TextEditingController departmentController;
  TextEditingController civilRegisteryController;
  final _formKey = GlobalKey<FormState>();
  final dateKey = GlobalKey<FormState>();
  bool buttonPressed = false;
  // String governorateDropDown;
  String token;
  String governorateId;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Provider.of<gov.Governorate>(context, listen: false)
          .fetchAndSetGovernorates(),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final provide = Provider.of<UserById>(context);
    fullNameController = TextEditingController(
      text: provide.userById?.user?.name,
    );
    nationalIdController = TextEditingController(
      text: provide.userById?.user?.nationalId,
    );

    addressController = TextEditingController(
      text: provide.userById?.user?.address,
    );

    mobileController = TextEditingController(
      text: provide.userById?.user?.phone,
    );
    landLineController = TextEditingController(
      text: provide.userById?.user?.landLine,
    );
    emailController = TextEditingController(
      text: provide.userById?.user?.email,
    );
    birthPlaceController = TextEditingController(
      text: provide.userById?.user?.studentProfile?.placeOfBirth,
    );
    departmentController = TextEditingController(
      text: provide.userById?.user?.studentProfile?.department,
    );
    civilRegisteryController = TextEditingController(
      text: provide.userById?.user?.studentProfile?.civilRegistryOffice,
    );
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting(
      "ar",
    );
    final DateFormat formatter = DateFormat.yMMMd('ar_SA');
    final institution = Provider.of<Institution>(context);
    final auth = Provider.of<Auth>(context);
    // final send = Provider.of<SendRequest>(context);
    final governorate = Provider.of<gov.Governorate>(context);
    final provide = Provider.of<UserById>(context);
    String governorateDropDown = provide.userById?.user?.governorate?.arTitle;
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('تعديل بياناتي'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'بيانات شخصية',
                            style: TITLE_STYLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'الاسم بالكامل',
                        hinttext: 'الاسم بالكامل',
                        controller: fullNameController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'تاريخ الميلاد',
                            style: SUBTITLE,
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      DateWidget(),
                      TextWidget(
                        text: 'جهة الميلاد',
                        hinttext: 'جهة الميلاد',
                        controller: birthPlaceController,
                      ),
                      TextWidget(
                        text: 'العنوان',
                        hinttext: 'أدخل العنوان',
                        controller: addressController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'المحافظة',
                            style: SUBTITLE,
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Consumer<gov.Governorate>(
                            builder:
                                (BuildContext context, model, Widget child) {
                              return Expanded(
                                child: Stack(
                                  children: [
                                    Container(
                                      height: 42,
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                      ),
                                    ),
                                    SearchableDropdown.single(
                                      value: governorateDropDown,
                                      validator: (value) {
                                        if ((value == null || value.isEmpty) &&
                                            buttonPressed) {
                                          return 'من فضلك ادخل المحافظة';
                                        }
                                        return null;
                                      },
                                      searchFn: institution.searchfn,
                                      items: model.governorates
                                          .map((gov.Governorate value) {
                                        return DropdownMenuItem(
                                          value: value.id,
                                          child: Text(value.arTitle),
                                        );
                                      }).toList(),
                                      // value: 'dropdownValueInstitutions',
                                      hint: "اختر المحافظة",
                                      searchHint: null,
                                      underline: Container(),
                                      onChanged: (value) {
                                        setState(() {
                                          governorateDropDown = value;
                                        });
                                      },
                                      isExpanded: true,
                                    ),
                                  ],
                                ),
                                // child: Container(
                                //   decoration: BoxDecoration(
                                //     border: Border.all(color: Colors.grey),
                                //     borderRadius: BorderRadius.circular(8.0),
                                //   ),
                                //   child: SearchableDropdown.single(
                                //     value: governorateDropDown,
                                //     validator: (value) {
                                //       if ((value == null || value.isEmpty) &&
                                //           buttonPressed) {
                                //         return 'من فضلك ادخل المحافظة';
                                //       }
                                //       return null;
                                //     },
                                //     searchFn: institution.searchfn,
                                //     items: model.governorates
                                //         .map((gov.Governorate value) {
                                //       return DropdownMenuItem(
                                //         value: value.id,
                                //         child: Text(value.arTitle),
                                //       );
                                //     }).toList(),
                                //     // value: 'dropdownValueInstitutions',
                                //     hint: "اختر المحافظة",
                                //     searchHint: null,
                                //     underline: Container(),
                                //     onChanged: (value) {
                                //       setState(() {
                                //         governorateDropDown = value;
                                //       });
                                //     },
                                //     isExpanded: true,
                                //   ),
                                // ),
                              );
                            },
                          ),
                        ],
                      ),
                      TextWidget(
                        text: 'القسم التابع له',
                        hinttext: 'أدخل القسم التابع له',
                        controller: departmentController,
                      ),
                      NationalidWidget(
                        text: 'الرقم القومي',
                        hinttext: 'الرقم القومي',
                        controller: nationalIdController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'مكتب السجل المدني',
                        hinttext: 'أدخل مكتب السجل المدني',
                        controller: civilRegisteryController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'المحافظة',
                            style: SUBTITLE,
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Consumer<gov.Governorate>(
                            builder:
                                (BuildContext context, model, Widget child) {
                              return Expanded(
                                  child: Stack(children: [
                                Container(
                                  height: 42,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                SearchableDropdown.single(
                                  value: governorateDropDown,
                                  validator: (value) {
                                    if ((value == null || value.isEmpty) &&
                                        buttonPressed) {
                                      return 'من فضلك ادخل المحافظة';
                                    }
                                    return null;
                                  },
                                  searchFn: institution.searchfn,
                                  items: model.governorates
                                      .map((gov.Governorate value) {
                                    return DropdownMenuItem(
                                      value: value.id,
                                      child: Text(value.arTitle),
                                    );
                                  }).toList(),
                                  // value: 'dropdownValueInstitutions',
                                  hint: "اختر المحافظة",
                                  searchHint: null,
                                  underline: Container(),
                                  onChanged: (value) {
                                    setState(() {
                                      governorateDropDown = value;
                                    });
                                  },
                                  isExpanded: true,
                                ),
                              ])
                                  // child: Container(
                                  //   decoration: BoxDecoration(
                                  //     border: Border.all(color: Colors.grey),
                                  //     borderRadius: BorderRadius.circular(8.0),
                                  //   ),
                                  //   child: SearchableDropdown.single(
                                  //     value: governorateDropDown,
                                  //     validator: (value) {
                                  //       if ((value == null || value.isEmpty) &&
                                  //           buttonPressed) {
                                  //         return 'من فضلك ادخل المحافظة';
                                  //       }
                                  //       return null;
                                  //     },
                                  //     searchFn: institution.searchfn,
                                  //     items: model.governorates
                                  //         .map((gov.Governorate value) {
                                  //       return DropdownMenuItem(
                                  //         value: value.id,
                                  //         child: Text(value.arTitle),
                                  //       );
                                  //     }).toList(),
                                  //     // value: 'dropdownValueInstitutions',
                                  //     hint: "اختر المحافظة",
                                  //     searchHint: null,
                                  //     underline: Container(),
                                  //     onChanged: (value) {
                                  //       setState(() {
                                  //         governorateDropDown = value;
                                  //       });
                                  //     },
                                  //     isExpanded: true,
                                  //   ),
                                  // ),
                                  );
                            },
                          ),
                        ],
                      ),
                      MobileWidget(
                        text: 'رقم الموبايل',
                        hinttext: 'رقم الموبايل',
                        controller: mobileController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'رقم التليفون الارضي',
                            style: SUBTITLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        controller: landLineController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'رقم التليفون الارضي',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      EmailWidget(
                        controller: emailController,
                        text: 'البريد الإلكتروني',
                        hinttext: 'البريد الإلكتروني',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(40, 47, 115, 1),
                  ),
                ),
                onPressed: () async {
                  setState(() {
                    buttonPressed = true;
                  });

                  if (_formKey.currentState.validate()) {
                    SendRequest.updateStudent(
                      token: auth.token,
                      student: Student(
                        username: fullNameController.text,
                        address: addressController.text,
                        civilRegistryOffice: civilRegisteryController.text,
                        department: departmentController.text,
                        email: emailController.text,
                        nationalId: nationalIdController.text,
                        phone: mobileController.text,
                        placeOfBirth: birthPlaceController.text,
                        landline: landLineController.text,
                        birthday: dateController.text,
                      ),
                    ).then((value) {
                      print('value $value');
                      if (value?.message == null) {
                        print('${auth.id}  ${auth.token}');
                        provide.getUserById(auth.id, auth.token);
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('Success'),
                          ),
                        );
                      } else {
                        Navigator.pushNamed(context, Fail.routeName);
                      }
                    });
                    // print('fax ' + faxController.text);'845922ab-a53e-4ef2-be68-71a272b15bb7'
                    // print(Task.date?.toIso8601String());
                    // print('date' + dateController.text);

                    // governorateId = await governorate
                    //     .getGovernorateIdByName(governorateDropDown);
                    // token = auth.token;
                    // send.updateStudent(
                    //   address: addressController.text,
                    //   nationalId: nationalIdController.text,
                    //   phone: mobileController.text,
                    //   username: fullNameController.text,
                    //   token: token,
                    //   landLine: landLineController.text,
                    //   email: emailController.text,
                    //   civilRegisteryOffice: civilRegisteryController.text,
                    //   department: departmentController.text,
                    //   placeOfBirth: birthPlaceController.text,
                    //   birthDay: dateController.text.toIso8601String(),
                    // )
                    //     .then((value) {
                    //   print('value $value');
                    //   if (value.message == null) {
                    //     provide.getUserById(auth.id, token);
                    //     ScaffoldMessenger.of(context).showSnackBar(
                    //       SnackBar(
                    //         content: Text('Success'),
                    //       ),
                    //     );
                    //     // Navigator.pushNamed(context, '/done');
                    //   } else {
                    //     Navigator.pushNamed(context, '/fail');
                    //   }
                    // });
                  }
                },
                child: Text('حفظ'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
