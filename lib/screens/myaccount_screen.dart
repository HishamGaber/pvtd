import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/screens/studentProfile.dart';
import '/providers/auth.dart';
import '/screens/signin.dart';
import 'companyProfile.dart';
import 'individualProfile.dart';

class Myaccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Auth auth = Provider.of<Auth>(context);
    if (auth.isLogin) {
      if (auth.isCompany) {
        return CompanyProfile();
      }
      if (auth.isIndividual) {
        return IndividualProfile();
      }
      if (auth.isStudent) {
        return StudentProfile();
      }
    } else {
      return Signin();
    }
  }
}
