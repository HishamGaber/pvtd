import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:pvtd_app/providers/services.dart';

import 'serviceDetails.dart';

class ServicesScreen extends StatefulWidget {
  static const String routeName = '/servicesScreen';
  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  String title;
  String description;
  var is_init = true;
  bool isLoading = true;
  @override
  void didChangeDependencies() {
    if (is_init) {
      Provider.of<Service>(context).fetchServices().then((_) {
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      });
    }
    is_init = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final provide = Provider.of<Service>(context);
    List<ServiceCard> serviceCards() {
      List<ServiceCard> list = [];
      //i<5, pass your dynamic limit as per your requirment

      if (provide.serviceResponse != null) {
        print(provide.serviceResponse.services.length);
        for (int i = 0; i < provide.serviceResponse.services.length; i++) {
          list.add(
            ServiceCard(
              title: provide.serviceResponse.services[i].arTitle,
              description: provide.serviceResponse.services[i].arDescription,
              type: provide.serviceResponse.services[i].serviceType,
            ),
          );
        }
        setState(() {
          isLoading = false;
        });
      }

      return list; // all widget added now retrun the list here
    }

    return Scaffold(
      backgroundColor: Colors.indigo[50],
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : ListView(
              children: [
                Column(
                  children: serviceCards(),
                ),
              ],
            ),
    );
  }
}

class ServiceCard extends StatelessWidget {
  ServiceCard({this.title, this.description, this.type}) : super();
  final String title;
  final String description;
  final String type;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        // elevation: 15,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text(
                title,
                style: TITLE_STYLE,
              ),
              subtitle: Html(
                data: description.substring(0, 150) + '...',
                style: {
                  "body": Style(
                    fontSize: FontSize(16.0),
                    fontWeight: FontWeight.normal,
                    lineHeight: LineHeight(1.8),
                  ),
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Stack(
                  children: [
                    CircleAvatar(
                      backgroundColor: Color.fromRGBO(40, 47, 115, 1),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ServiceDetails(
                              text: description,
                              title: title,
                              type: type,
                            ),
                          ),
                        );
                      },
                      icon: Padding(
                        padding: const EdgeInsets.only(bottom: 7, left: 7),
                        child: Icon(
                          Icons.arrow_forward_outlined,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
