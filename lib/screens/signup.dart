import 'package:flutter/material.dart';
import 'package:pvtd_app/screens/signin.dart';
import 'package:pvtd_app/screens/signupCompany.dart';
import 'package:pvtd_app/screens/signupIndividual.dart';
import 'package:pvtd_app/screens/signupStudent.dart';

class Signup extends StatelessWidget {
  static const String routeName = '/signUp';
  // FocusNode _focusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        reverse: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 40,
            ),
            Text(
              "إنشاء حساب",
              style: const TextStyle(
                color: const Color(0xff343433),
                fontWeight: FontWeight.w700,
                fontFamily: "Almarai",
                fontStyle: FontStyle.normal,
                fontSize: 24.0,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Text(
              "هل انت؟",
              style: const TextStyle(
                  color: const Color(0xffbfbfbf),
                  fontWeight: FontWeight.w400,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 18.0),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(SignupStudent.routeName);
                },
                child: Text('طالب'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color.fromRGBO(45, 54, 147, 1)),
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(SignupCompany.routeName);
                },
                child: Text('شركة'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color.fromRGBO(45, 54, 147, 1)),
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(SignupIndividual.routeName);
                },
                child: Text('فرد'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color.fromRGBO(45, 54, 147, 1)),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  " لديك حساب؟ ",
                  style: const TextStyle(
                    color: const Color(0xffb7b7b8),
                    fontWeight: FontWeight.w400,
                    fontFamily: "Almarai",
                    fontStyle: FontStyle.normal,
                    fontSize: 14.0,
                  ),
                  textAlign: TextAlign.right,
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(Signin.routeName);
                  },
                  child: Text(
                    "سجل دخول الأن",
                    style: const TextStyle(
                      color: const Color(0xff2d3693),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
