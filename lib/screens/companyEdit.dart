import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/company.dart';
import 'package:pvtd_app/providers/governorate.dart' as gov;
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/userById.dart';
import 'package:pvtd_app/widgets/emailWidget.dart';
import 'package:pvtd_app/widgets/mobileWidget.dart';
import 'package:pvtd_app/widgets/nationalidWidget.dart';
import 'package:pvtd_app/widgets/textWidget.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'fail.dart';

class CompanyEdit extends StatefulWidget {
  static const String routeName = '/companyEdit';
  @override
  _CompanyEditState createState() => _CompanyEditState();
}

class _CompanyEditState extends State<CompanyEdit> {
  TextEditingController fullNameController;
  TextEditingController nationalIdController;
  TextEditingController commercialRegisterController;
  TextEditingController companyNameController;
  TextEditingController addressController;
  TextEditingController emailController;
  TextEditingController faxController;
  TextEditingController landLineController;
  TextEditingController mobileController;
  final _formKey = GlobalKey<FormState>();
  String governorateDropDown;
  String token;
  String governorateId;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Provider.of<gov.Governorate>(context, listen: false)
          .fetchAndSetGovernorates(),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final provide = Provider.of<UserById>(context);
    fullNameController = TextEditingController(
      text: provide.userById?.user?.name,
    );
    nationalIdController = TextEditingController(
      text: provide.userById?.user?.nationalId,
    );
    commercialRegisterController = TextEditingController(
      text: provide.userById?.user?.companyProfile?.commercialRegister,
    );
    companyNameController = TextEditingController(
      text: provide.userById?.user?.companyProfile?.companyName,
    );
    addressController = TextEditingController(
      text: provide.userById?.user?.address,
    );
    emailController = TextEditingController(
      text: provide.userById?.user?.email,
    );
    faxController = TextEditingController(
        text: provide.userById?.user?.companyProfile?.fax);

    mobileController = TextEditingController(
      text: provide.userById?.user?.phone,
    );
    landLineController = TextEditingController(
      text: provide.userById?.user?.landLine,
    );
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    final auth = Provider.of<Auth>(context);
    // final send = Provider.of<SendRequest>(context);
    final governorate = Provider.of<gov.Governorate>(context);
    final provide = Provider.of<UserById>(context);
    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('تعديل بياناتي'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'بيانات شخصية',
                            style: TITLE_STYLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'الاسم بالكامل',
                        hinttext: 'الاسم بالكامل',
                        controller: fullNameController,
                      ),
                      NationalidWidget(
                        text: 'الرقم القومي',
                        hinttext: 'الرقم القومي',
                        controller: nationalIdController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'رقم السجل التجاري',
                            style: SUBTITLE,
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'من فضلك ادخل رقم';
                          }

                          return null;
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.right,
                        controller: commercialRegisterController,
                        decoration: InputDecoration(
                          hintText: 'ادخل رقم السجل التجاري',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'اسم الشركة',
                        hinttext: 'أدخل اسم الشركة',
                        controller: companyNameController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'المحافظة',
                            style: SUBTITLE,
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Consumer<gov.Governorate>(
                            builder:
                                (BuildContext context, model, Widget child) {
                              return Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: SearchableDropdown.single(
                                    value: governorateDropDown,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'من فضلك ادخل المحافظة';
                                      }
                                      return null;
                                    },
                                    searchFn: institution.searchfn,
                                    items: model.governorates
                                        .map((gov.Governorate value) {
                                      return DropdownMenuItem(
                                        value: value.id,
                                        child: Text(value.arTitle),
                                      );
                                    }).toList(),
                                    // value: 'dropdownValueInstitutions',
                                    hint: "اختر المحافظة",
                                    searchHint: null,
                                    underline: Container(),
                                    onChanged: (value) {
                                      setState(() {
                                        governorateDropDown = value;
                                      });
                                    },
                                    isExpanded: true,
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextWidget(
                        text: 'العنوان',
                        hinttext: 'أدخل العنوان',
                        controller: addressController,
                      ),
                      EmailWidget(
                        controller: emailController,
                        text: 'البريد الإلكتروني',
                        hinttext: 'البريد الإلكتروني',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'الفاكس',
                            style: SUBTITLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'من فضلك ادخل رقم';
                          }
                          return null;
                        },
                        keyboardType: TextInputType.number,
                        controller: faxController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'الفاكس',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MobileWidget(
                        text: 'رقم الموبايل',
                        hinttext: 'رقم الموبايل',
                        controller: mobileController,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'رقم التليفون الارضي',
                            style: SUBTITLE,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        // validator: (value) {
                        //   if (value == null || value.isEmpty) {
                        //     return 'من فضلك ادخل رقم';
                        //   }
                        //   return null;
                        // },
                        keyboardType: TextInputType.number,
                        controller: landLineController,
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'رقم التليفون الارضي',
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(40, 47, 115, 1),
                  ),
                ),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    SendRequest.updateCompany(
                      token: auth.token,
                      company: Company(
                        address: addressController.text,
                        commercialRegister: commercialRegisterController.text,
                        companyName: companyNameController.text,
                        email: emailController.text,
                        fax: faxController.text,
                        governorateId: governorateDropDown,
                        landline: landLineController.text,
                        nationalId: nationalIdController.text,
                        phone: mobileController.text,
                        username: fullNameController.text,
                      ),
                    ).then((value) {
                      print('value $value');
                      if (value?.message == null) {
                        print('${auth.id}  ${auth.token}');
                        provide.getUserById(auth.id, auth.token);
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('Success'),
                          ),
                        );
                      } else {
                        Navigator.pushNamed(context, Fail.routeName);
                      }
                    });
                    // print('fax ' + faxController.text);'845922ab-a53e-4ef2-be68-71a272b15bb7'
                    // governorateId = await governorate
                    //     .getGovernorateIdByName(governorateDropDown);
                    // token = auth.token;
                    // send
                    //     .updateCompany(
                    //   address: addressController.text,
                    //   commercialRegister: commercialRegisterController.text,
                    //   companyName: companyNameController.text,
                    //   email: emailController.text,
                    //   fax: faxController.text,
                    //   nationalId: nationalIdController.text,
                    //   phone: mobileController.text,
                    //   username: fullNameController.text,
                    //   token: token,
                    //   landline: landLineController.text,
                    //   governorateId: governorateId,
                    // )
                    //     .then((value) {
                    //   print('value $value');
                    //   if (value.message == null) {
                    //     provide.getUserById(auth.id, token);
                    //     ScaffoldMessenger.of(context).showSnackBar(
                    //       SnackBar(
                    //         content: Text('Success'),
                    //       ),
                    //     );
                    //     // Navigator.pushNamed(context, '/done');
                    //   } else {
                    //     Navigator.pushNamed(context, '/fail');
                    //   }
                    // });
                  }
                },
                child: Text('حفظ'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
