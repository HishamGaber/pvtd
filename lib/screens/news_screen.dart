import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/news.dart';
import 'newsDetails.dart';

class NewsScreen extends StatefulWidget {
  static const String routeName = '/newsScreen';
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  var is_init = true;
  bool isLoading = true;
  @override
  void didChangeDependencies() {
    if (is_init) {
      Provider.of<News>(context).fetchAndSetNews().then((_) {
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      });
    }
    is_init = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final provide = Provider.of<News>(context);
    List<NewsCard> newsCards() {
      List<NewsCard> list = [];
      //i<5, pass your dynamic limit as per your requirment

      if (provide.newsResponse != null) {
        // print(provide.newsResponse.news.length);
        for (int i = 0; i < provide.newsResponse.news.length; i++) {
          DateTime date = provide.newsResponse.news[i].updatedAt;
          initializeDateFormatting(
            "ar",
          );
          final DateFormat formatter =
              DateFormat.yMEd('ar_SA').addPattern(' - ').add_Hm();
          final String formatted = formatter.format(date);
          print(formatted);
          list.add(
            NewsCard(
              title: provide.newsResponse.news[i].title,
              description: provide.newsResponse.news[i].content,
              imageUrl: provide.newsResponse.news[i].coverImg,
              date: formatted,
            ),
          );
        }

        setState(() {
          isLoading = false;
        });
      }
      return list;
      // all widget added now retrun the list here
    }

    return Scaffold(
      backgroundColor: Colors.indigo[50],
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : ListView(
              children: [
                Column(
                  children: newsCards(),
                ),
              ],
            ),
    );
  }
}

class NewsCard extends StatelessWidget {
  NewsCard({
    this.title,
    this.description,
    this.imageUrl,
    this.date,
  }) : super();
  final String title;
  final String description;
  final String imageUrl;
  final String date;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => NewsDetails(
                        text: description,
                        title: title,
                        image: imageUrl,
                      )),
            );
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.network(imageUrl),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.access_time,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        date,
                      ),
                    ],
                  ),
                ),
                ListTile(
                  title: Text(
                    title,
                    style: TITLE_STYLE,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  subtitle: Html(
                    data: (description.length > 210)
                        ? description.substring(0, 210) + '...'
                        : description + '...',
                    style: {
                      "body": Style(
                        fontSize: FontSize(16.0),
                        fontWeight: FontWeight.normal,
                        lineHeight: LineHeight(1.8),
                      ),
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
