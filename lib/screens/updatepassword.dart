import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/password.dart';
import 'package:pvtd_app/providers/sendRequest.dart';

class UpdatePassword extends StatelessWidget {
  static const String routeName = '/updatePassword';
  @override
  Widget build(BuildContext context) {
    TextEditingController currentPasswordController = TextEditingController();
    TextEditingController passswordController = TextEditingController();
    TextEditingController confirmPasswordController = TextEditingController();

    // final bottom = MediaQuery.of(context).viewInsets.bottom;
    // final send = Provider.of<SendRequest>(context);
    final auth = Provider.of<Auth>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        reverse: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 40,
            ),
            Image(
              image: AssetImage('assets/images/bitmap.png'),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "أعد تعيين كلمة المرور",
              style: TITLE_STYLE,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: TextFormField(
                controller: currentPasswordController,
                textAlign: TextAlign.right,
                decoration: InputDecoration(
                  hintText: 'كلمة المرور القديمة',
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: TextFormField(
                controller: passswordController,
                textAlign: TextAlign.right,
                decoration: InputDecoration(
                  hintText: 'كلمة المرور الجديدة',
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: TextFormField(
                controller: confirmPasswordController,
                textAlign: TextAlign.right,
                decoration: InputDecoration(
                  hintText: 'أعد إدخال كلمة المرور الجديدة',
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 327,
              height: 58,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  color: const Color(0xff2d3693)),
              child: ElevatedButton(
                onPressed: () {
                  SendRequest.updatePassword(
                    password: Password(
                      currentPassword: currentPasswordController.text,
                      password: passswordController.text,
                    ),
                    token: auth.token,
                  ).then((value) {
                    print('value $value');
                    if (value.message == 'success') {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text('Success'),
                        ),
                      );
                      // Navigator.pushNamed(context, '/done');
                    } else {
                      Navigator.pushNamed(context, '/fail');
                    }
                  });
                  // send
                  //     .updatePassword(
                  //   currentPassword: currentPasswordController.text,
                  //   pasword: passswordController.text,
                  //   confirmPassword: confirmPasswordController.text,
                  //   token: auth.token,
                  // )
                  //     .then((value) {
                  //   print('value $value');
                  //   if (value.message == null) {
                  //     ScaffoldMessenger.of(context).showSnackBar(
                  //       SnackBar(
                  //         content: Text('Success'),
                  //       ),
                  //     );
                  //     // Navigator.pushNamed(context, '/done');
                  //   } else {
                  //     Navigator.pushNamed(context, '/fail');
                  //   }
                  // });
                },
                child: Text('ارسل كلمة المرور'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Color.fromRGBO(45, 54, 147, 1)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
