import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/screens/services/certificate_service.dart';
import 'package:pvtd_app/screens/services/consolation_service.dart';
import 'package:pvtd_app/screens/services/maintenance_service.dart';
import 'package:pvtd_app/screens/services/manifacturing_service.dart';
import 'package:pvtd_app/screens/services/skill_service.dart';
import 'package:pvtd_app/screens/services/station_service.dart';
import 'package:pvtd_app/screens/services/trainingApply.dart';
import 'package:pvtd_app/screens/services/training_service.dart';
import 'package:pvtd_app/screens/signin.dart';

class ServiceDetails extends StatelessWidget {
  final String text;
  final String title;
  final String type;
  ServiceDetails({
    this.text,
    this.title,
    this.type,
  }) : super();
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Auth>(context);

    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Color.fromRGBO(45, 54, 147, 1),
        title: Text(this.title),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Html(
                data: this.text,
                style: {
                  "body": Style(
                    fontSize: FontSize(16.0),
                    fontWeight: FontWeight.normal,
                    lineHeight: LineHeight(1.8),
                  ),
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 60,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  ),
                ),
                onPressed: () {
                  switch (type) {
                    case 'applying_to_training_centers':
                      {
                        if (auth.isLogin) {
                          Navigator.pushNamed(context, TrainingApply.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'certificates':
                      {
                        if (auth.isLogin) {
                          Navigator.pushNamed(
                              context, CertificateService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'training_services':
                      {
                        if (auth.isLogin) {
                          Navigator.pushNamed(
                              context, TrainingService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'measurement_skill_level':
                      {
                        if (auth.isLogin) {
                          Navigator.of(context)
                              .pushNamed(SkillService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'manufacturing_for_others':
                      {
                        if (auth.isLogin) {
                          Navigator.of(context)
                              .pushNamed(ManufacturingService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'maintenance':
                      {
                        if (auth.isLogin) {
                          Navigator.of(context)
                              .pushNamed(MaintenanceService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'setup_station':
                      {
                        if (auth.isLogin) {
                          Navigator.of(context)
                              .pushNamed(StationService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    case 'consolation_services':
                      {
                        if (auth.isLogin) {
                          Navigator.of(context)
                              .pushNamed(ConsolationService.routeName);
                        } else {
                          Navigator.pushNamed(context, Signin.routeName);
                        }
                      }
                      break;
                    default:
                  }
                },
                child: Text('قدم الآن'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
