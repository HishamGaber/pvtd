import 'package:flutter/material.dart';

class Contact extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ContactState();
  }
}

class ContactState extends State<Contact> {
  static final formKey = GlobalKey<FormState>();
  static TextEditingController companyNameController = TextEditingController();
  static TextEditingController companyActivityController =
      TextEditingController();
  static TextEditingController companyPhoneController = TextEditingController();
  static TextEditingController faxController = TextEditingController();
  static TextEditingController addressController = TextEditingController();
  static TextEditingController jobController = TextEditingController();
  static TextEditingController suggestedProgramController =
      TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
      key: formKey,
      autovalidate: false,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'بيانات العمل',
                style: const TextStyle(
                  color: const Color(0xff26262b),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('اسم الشركة',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            maxLines: 1,
            controller: companyNameController,
            decoration: InputDecoration(
              hintText: 'اسم الشركة',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل اسم الشركة";
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('نشاطها',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'نشاط الشركة',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل نشاط الشركة";
              }
              return null;
            },
            controller: companyActivityController,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('التليفون',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل التليفون";
              }
              return null;
            },
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'التليفون',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            controller: companyPhoneController,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('فاكس',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل الفاكس";
              }
              return null;
            },
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'الفاكس',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            controller: faxController,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('العنوان',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'العنوان',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل العنوان";
              }
            },
            controller: addressController,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('الوظيفة القائم بها',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل الوظيفة";
              }
            },
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'الوظيفة القائم بها',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            controller: jobController,
          ),
        ],
      ),
    ));
  }
}
