import 'package:flutter/material.dart';

class MobileWidget extends StatelessWidget {
  final String text;
  final String hinttext;
  final TextEditingController controller;

  const MobileWidget({
    @required this.text,
    @required this.hinttext,
    @required this.controller,
  }) : super();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(text,
                style: const TextStyle(
                  color: const Color(0xff26262b),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 16.0,
                ),
                textAlign: TextAlign.right),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'من فضلك ادخل رقم';
            }
            if (value.length < 11) {
              return 'ادخل رقم موبايل صحيح';
            }
            return null;
          },
          keyboardType: TextInputType.number,
          controller: controller,
          textAlign: TextAlign.right,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
