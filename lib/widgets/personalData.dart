import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/governorate.dart' as gov;
import 'package:pvtd_app/providers/governorate.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:pvtd_app/widgets/textWidget.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class PersonalData extends StatefulWidget {
  @override
  PersonalDataState createState() => PersonalDataState();
}

class PersonalDataState extends State<PersonalData> {
  static TextEditingController militaryCardController = TextEditingController();
  static String governorateDropDown;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Provider.of<gov.Governorate>(context, listen: false)
          .fetchAndSetGovernorates(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          TextWidget(
            text: 'رقم بطاقة التجنيد',
            hinttext: 'أدخل رقم البطاقة',
            controller: militaryCardController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                ' محافظة التجنيد التابع لها',
                style: SUBTITLE,
                textAlign: TextAlign.right,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              DefaultGovernorateDropDown(
                onChanged: (Governorate value) {
                  setState(() {
                    governorateDropDown = value.id;
                  });
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
