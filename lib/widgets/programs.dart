import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/tracks.dart';
import 'package:pvtd_app/providers/training.dart';
import 'package:pvtd_app/screens/done.dart';
import 'package:pvtd_app/screens/fail.dart';
import 'package:pvtd_app/widgets/institutionData.dart';
import 'package:pvtd_app/widgets/qualification.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'contact.dart';

class Programs extends StatefulWidget {
  @override
  _ProgramsState createState() => _ProgramsState();
}

class _ProgramsState extends State<Programs> {
  static final formKey = GlobalKey<FormState>();
  String dropdownValueInstitutionsTracks;
  String dropdownValueTrack;
  static TextEditingController suggestedProgram = TextEditingController();
  String _token;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    final auth = Provider.of<Auth>(context);
    final institutionTraining = Provider.of<InstitutionTraining>(context);
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    'البرامج التي ترغب في التدريب عليها',
                    style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 24.0,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'نوع البرنامج',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Consumer<InstitutionTraining>(
                    builder: (BuildContext context, model, Widget child) {
                      return Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: SearchableDropdown.single(
                          searchFn: institution.searchfn,
                          items: model.tracksOfInstitution.isNotEmpty
                              ? model.tracksOfInstitution
                                  ?.map((InstitutionTraining value) {
                                  return DropdownMenuItem(
                                    value: value.eductionTrackId,
                                    child: Text(value?.eductionTrack?.arTitle),
                                  );
                                })?.toList()
                              : [],
                          value: dropdownValueInstitutionsTracks,
                          hint: "نوع البرنامج",
                          searchHint: null,
                          underline: Container(),
                          onChanged: (value) {
                            setState(() {
                              dropdownValueInstitutionsTracks = value;
                              institutionTraining.fetchAndSetPrograms(
                                  dropdownValueInstitutionsTracks);
                            });
                          },
                          isExpanded: true,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'اختر البرنامج',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Consumer<InstitutionTraining>(
                    builder: (BuildContext context, model, Widget child) {
                      return Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: SearchableDropdown.single(
                          searchFn: institution.searchfn,
                          items:
                              model.programs.map((InstitutionTraining value) {
                            return DropdownMenuItem(
                              value: value.id,
                              child: Text(
                                  value?.eductionTrack?.trackType?.arTitle),
                            );
                          }).toList(),
                          value: dropdownValueTrack,
                          hint: "اختر البرنامج",
                          searchHint: null,
                          underline: Container(),
                          onChanged: (value) {
                            setState(() {
                              dropdownValueTrack = value;
                            });
                          },
                          isExpanded: true,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'اقترح برامج اخرى',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              maxLines: 1,
              controller: suggestedProgram,
              decoration: InputDecoration(
                hintText: 'ادخل اقتراحات اخرى',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
              ),
              validator: (value) {
                if (value.trim().isEmpty) {
                  return "من فضلك ادخل المؤهل";
                }
                return null;
              },
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  '* الحد الأدني للمتدريبين 4 متدربيبين والحد\n الإقصى 16 متدرب لكل برنامج',
                  style: const TextStyle(
                    color: const Color(0xff26262b),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Almarai",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0,
                  ),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                    '* عند استكمال عدد المتدربين بالدورة سوف\n يتم الاتصال بك لتأكيد الميعاد',
                    style: const TextStyle(
                        color: const Color(0xff26262b),
                        fontWeight: FontWeight.w700,
                        fontFamily: "Almarai",
                        fontStyle: FontStyle.normal,
                        fontSize: 16.0),
                    textAlign: TextAlign.start),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    height: 60,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          Color.fromRGBO(45, 54, 147, 1),
                        ),
                      ),
                      onPressed: () async {
                        SendRequest.sendTraining(
                          token: auth.token,
                          training: Training(
                              educationalQualification: QualificationState
                                  .educationalQualification.text,
                              educationalQualificationYear: QualificationState
                                  .educationalQualificationYear.text,
                              educationalSpecialization: QualificationState
                                  .educationalSpecialization.text,
                              higherQualification:
                                  QualificationState.higherQualification.text,
                              higherQualificationYear: QualificationState
                                  .higherQualificationYear.text,
                              higherSpecialization:
                                  QualificationState.higherSpecialization.text,
                              companyName:
                                  ContactState.companyNameController.text,
                              companyActivity:
                                  ContactState.companyActivityController.text,
                              companyPhone:
                                  ContactState.companyPhoneController.text,
                              fax: ContactState.faxController.text,
                              address: ContactState.addressController.text,
                              job: ContactState.jobController.text,
                              suggestedProgram: dropdownValueTrack,
                              institutionTrackId:
                                  QualificationState.dropdownValueInstitutions),
                        ).then((value) {
                          if (value?.message == null) {
                            Navigator.pushNamed(context, Done.routeName);
                          } else {
                            Navigator.pushNamed(context, Fail.routeName);
                          }
                        });
                      },
                      child: Text('قدم الآن'),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
