import 'package:flutter/material.dart';

DateTime date;

TextEditingController dateController = TextEditingController();
DateTime selectedDate = DateTime.now();

class DateWidget extends StatefulWidget {
  // DateTime selectedDate ;
  @override
  _DateWidgetState createState() => _DateWidgetState();
}

class _DateWidgetState extends State<DateWidget> {
  final _formKey = GlobalKey<FormState>();
  // TextEditingController dateController = TextEditingController();
  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        // var date =
        //     "${picked.toLocal().day}/${picked.toLocal().month}/${picked.toLocal().year}";
        // dateController.text =
        //     "${picked.toLocal().day}/${picked.toLocal().month}/${picked.toLocal().year}";
        dateController.text =
            '${selectedDate.day}/${selectedDate.month}/${selectedDate.year}';
      });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () => _selectDate(context),
          child: AbsorbPointer(
            child: TextFormField(
              key: _formKey,
              onSaved: (val) {
                date = selectedDate;
              },
              decoration: InputDecoration(
                labelText: "التاريخ",
                suffixIcon: Icon(Icons.calendar_today),
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
              controller: dateController,
              validator: (value) {
                if (value.isEmpty) return "من فضلك ادخل التاريخ";
                return null;
              },
            ),
          ),
        ),
      ],
    );
  }
}
