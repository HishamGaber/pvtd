import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/api_urls.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/governorate.dart';
import 'package:pvtd_app/providers/imageResponse.dart';
import 'package:pvtd_app/providers/sendRequest.dart';
import 'package:pvtd_app/providers/trainingApplication.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:pvtd_app/screens/done.dart';
import 'package:pvtd_app/screens/fail.dart';
import 'package:pvtd_app/widgets/dateWidget.dart';
import 'package:pvtd_app/widgets/institutionData.dart';
import 'package:pvtd_app/widgets/personalData.dart';
import 'package:pvtd_app/widgets/textWidget.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'package:async/async.dart';

import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http_parser/http_parser.dart';

// import 'institutionData.dart';

class EducationData extends StatefulWidget {
  @override
  _EducationDataState createState() => _EducationDataState();
}

class _EducationDataState extends State<EducationData> {
  TextEditingController certificateController = TextEditingController();
  String dropDownLanguage;
  TextEditingController totalController = TextEditingController();
  bool image1Uploaded = false;
  bool image2Uploaded = false;
  ImageResponse imageResponse;
  bool _check1 = false;
  bool _check2 = false;
  bool _check3 = false;
  String birthCertificate;
  String preporatoryCertificate;
  bool isButtonDisabled = true;

  Future<void> _takePicture() async {
    final picker = ImagePicker();
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
    var imageFile = File(pickedFile?.path);

    final appDir = await syspaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    // setState(() {
    //   name = fileName;
    // });

    upload(
      File(imageFile.path),
    );
  }

  Future<void> upload(File imageFile) async {
    // open a bytestream
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    // get file length
    var length = await imageFile.length();

    // string to uri
    var uri = Uri.parse(MAIN_URL + "upload/image");

    var request = new http.MultipartRequest("POST", uri);
    request.files.add(http.MultipartFile(
      'image',
      stream,
      length,
      filename: basename(imageFile.path),
      contentType: MediaType('image', 'jpg'),
    ));
    request.send().then((response) async {
      final respStr = await response.stream.bytesToString();
      var jj = json.decode(respStr);
      imageResponse = ImageResponse.fromJson(jj);
      print(imageResponse.fileUrl);
      if (response.statusCode == 200) print("Uploaded!");
    });
  }

  @override
  Widget build(BuildContext context) {
    final governorate = Provider.of<Governorate>(context);
    // final send = Provider.of<SendRequest>(context);
    final auth = Provider.of<Auth>(context);
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'بيانات التعليم',
                      style: TITLE_STYLE,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                TextWidget(
                  text: 'الشهادة الحاصل عليها',
                  hinttext: 'الشهادة الحاصل عليها',
                  controller: certificateController,
                ),
                Row(
                  children: [
                    Text(
                      'تاريخ الحصول عليها',
                      style: SUBTITLE,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                DateWidget(),
                SizedBox(
                  height: 10,
                ),
                DefaultLanguageDropDown(
                  label: 'اللغة الأجنبية   ',
                  onChanged: (value) {
                    setState(() {
                      dropDownLanguage = value;
                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'مجموع الدرجات الكلي',
                      style: SUBTITLE,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'من فضلك ادخل مجموع الدرجات الكلي ';
                    }

                    return null;
                  },
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.right,
                  controller: totalController,
                  decoration: InputDecoration(
                    hintText: 'مجموع الدرجات الكلي',
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'اوراق مطلوبة',
                  style: TITLE_STYLE,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'شهادة ميلاد كمبيوتر',
                  style: SUBTITLE,
                ),
              ],
            ),
            OutlinedButton(
              onPressed: () async {
                await _takePicture();

                setState(() {
                  image1Uploaded = true;
                  birthCertificate = imageResponse?.fileName;
                });
              },
              child: Column(
                children: [
                  Image(
                    image: AssetImage('assets/images/cloudComputing.png'),
                  ),
                  Text('قم برفع الملف'),
                ],
              ),
            ),
            image1Uploaded
                ? Row(
                    children: [
                      Expanded(child: Text('$birthCertificate')),
                    ],
                  )
                : Text(''),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'شهادة الاعدادية الاصلية',
                  style: SUBTITLE,
                ),
              ],
            ),
            OutlinedButton(
              onPressed: () async {
                await _takePicture();
                setState(() {
                  image2Uploaded = true;
                  preporatoryCertificate = imageResponse?.fileName;
                });
              },
              child: Column(
                children: [
                  Image(
                    image: AssetImage('assets/images/cloudComputing.png'),
                  ),
                  Text('قم برفع الملف'),
                ],
              ),
            ),
            image2Uploaded
                ? Row(
                    children: [
                      Expanded(child: Text('$preporatoryCertificate')),
                    ],
                  )
                : Text(''),
          ],
        ),
        Column(
          children: [
            CheckboxListTile(
              title: Text(
                'اقر بأنني على استعداد لتأدية الاختبارات النفسية والطبية التي تجريها المصلحة',
              ),
              value: _check1,
              onChanged: (value) {
                setState(() {
                  _check1 = !_check1;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,
            ),
            CheckboxListTile(
              title: Text(
                  'اقر بأنني تسلمت بطاقة هذه الاختبارات مبينا بها تاريخ ومكان اجراءها'),
              value: _check2,
              onChanged: (value) {
                setState(() {
                  _check2 = !_check2;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,
            ),
            CheckboxListTile(
              title: Text(
                  'اتعهد بسحب اوراقي من مكتب القبول في حال عدم اجتيازي الاختبارات النفسية والطبية'),
              value: _check3,
              onChanged: (value) {
                setState(() {
                  _check3 = !_check3;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.all(11.0),
              child: SizedBox(
                height: 60,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                    Color.fromRGBO(45, 54, 147, 1),
                  )),
                  onPressed: (_check1 && _check2 && _check3)
                      ? () async {
                          print('institutiontrack list');
                          print(InstitutionDataState.finalTracks[0].order);
                          SendRequest.sendTrainingApplication(
                            token: auth.token,
                            trainingApplication: TrainingApplication(
                              InstitutionTracks:
                                  InstitutionDataState.finalTracks,
                              recruitingCardNumber:
                                  PersonalDataState.militaryCardController.text,
                              recruitingGovernorateId:
                                  PersonalDataState.governorateDropDown,
                              certificate: certificateController.text,
                              computerBirthCertificate: birthCertificate,
                              originalPrepCertificate: preporatoryCertificate,
                              graduationDate: dateController.text,
                              grade: totalController.text,
                              language: dropDownLanguage,
                            ),
                          ).then((value) {
                            print('value  $value');
                            if (value.message == null) {
                              Navigator.pushNamed(context, Done.routeName);
                            } else {
                              Navigator.pushNamed(context, Fail.routeName);
                            }
                          });
                        }
                      : null,
                  child: Text('ارسل'),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
