import 'package:flutter/material.dart';

class ConfirmpasswordWidget extends StatelessWidget {
  final String text;
  final String hinttext;
  final TextEditingController controller;

  const ConfirmpasswordWidget({
    @required this.text,
    @required this.hinttext,
    @required this.controller,
  }) : super();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              text,
              style: const TextStyle(
                color: const Color(0xff26262b),
                fontWeight: FontWeight.w700,
                fontFamily: "Almarai",
                fontStyle: FontStyle.normal,
                fontSize: 16.0,
              ),
              textAlign: TextAlign.right,
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          validator: (value) {
            if (value != controller.text) {
              return 'كلمة المرور لا تتطابق';
            }
            return null;
          },
          obscureText: true,
          textAlign: TextAlign.right,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
