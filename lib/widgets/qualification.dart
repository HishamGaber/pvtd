import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/tracks.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class Qualification extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QualificationState();
  }
}

var idInstitution;
var trackTypeId;
String eduQualification;
String eduQualificationYear;
String eduSpecialization;
String highQualification;
String highQualificationYear;
String highSpecialization;
// String _dropValueInstitutions;

// String get dropValueInstitutions => _dropValueInstitutions;

class QualificationState extends State<Qualification> {
  static final formKey = GlobalKey<FormState>();
  static TextEditingController educationalQualification =
      TextEditingController();
  static TextEditingController educationalQualificationYear =
      TextEditingController();
  static TextEditingController educationalSpecialization =
      TextEditingController();
  static TextEditingController higherQualification = TextEditingController();
  static TextEditingController higherQualificationYear =
      TextEditingController();
  static TextEditingController higherSpecialization = TextEditingController();
  static String dropdownValueInstitutions;
  var institutionTraining;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<Institution>(context, listen: false)
            .fetchAndSetInstitutions());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    institutionTraining = Provider.of<InstitutionTraining>(context);
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    return Container(
        child: Form(
      key: formKey,
      autovalidate: false,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'اختر المركز',
                style: const TextStyle(
                  color: const Color(0xff26262b),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          DefaultInstitutionDropDown(
              label: 'المركز',
              onChanged: (Institution value) {
                dropdownValueInstitutions = value.id;
              }),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'المؤهلات العلمية',
                style: const TextStyle(
                  color: const Color(0xff26262b),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Almarai",
                  fontStyle: FontStyle.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'المؤهل الدراسي',
                style: const TextStyle(
                    color: const Color(0xff26262b),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Almarai",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0),
                textAlign: TextAlign.start,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            maxLines: 1,
            controller: educationalQualification,
            decoration: InputDecoration(
              hintText: 'المؤهل الدراسي',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
            ),
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل المؤهل";
              }
              return null;
            },
            onSaved: (String value) {
              eduQualification = value;
            },
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('التخصص',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'التخصص',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل التخصص";
              }
              return null;
            },
            controller: educationalSpecialization,
          ),
          SizedBox(height: 20),
          YearWidget(
            text: 'العام',
            hinttext: 'العام',
            controller: educationalQualificationYear,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('المؤهل الأعلى',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
              validator: (value) {
                if (value.trim().isEmpty) {
                  return "من فضلك ادخل المـؤهل الأعلى";
                }
                return null;
              },
              maxLines: 1,
              decoration: InputDecoration(
                hintText: 'المؤهل الأعلى',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
              ),
              controller: higherQualification),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('التخصص',
                  style: const TextStyle(
                      color: const Color(0xff26262b),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Almarai",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.start),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'التخصص',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            validator: (value) {
              if (value.trim().isEmpty) {
                return "من فضلك ادخل التخصص";
              }
              return null;
            },
            controller: higherSpecialization,
          ),
          SizedBox(height: 20),
          YearWidget(
            text: 'العام',
            hinttext: 'العام',
            controller: higherQualificationYear,
          ),
        ],
      ),
    ));
  }
}
