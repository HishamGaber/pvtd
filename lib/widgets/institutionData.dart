import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pvtd_app/common/styles.dart';
import 'package:pvtd_app/providers/auth.dart';
import 'package:pvtd_app/providers/Track.dart';
import 'package:pvtd_app/providers/institutions.dart';
import 'package:pvtd_app/providers/trackObject.dart';
import 'package:pvtd_app/providers/tracks.dart';
import 'package:pvtd_app/reusableComponents/reusableComponents.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class InstitutionData extends StatefulWidget {
  @override
  InstitutionDataState createState() => InstitutionDataState();
}

class InstitutionDataState extends State<InstitutionData> {
  String institutionId;
  String trackId;
  static String dropdownValueInstitutions;
  String _token;
  static InstitutionTraining dropdownValueTrack;
  var formKey = GlobalKey<FormState>();
  var institutionTraining;
  static List<Track> institutionTracks = [];
  static List<TrackObject> finalTracks = [];
  // List<InstitutionTrack> get institutionTracks => _institutionTracks;
  static List<Widget> trackCardList = [];
  void removeTrackCard(trackId) {
    if (mounted) {
      setState(() {
        trackCardList.remove(trackId);
        institutionTracks
            .removeWhere((element) => element.institutionTrackId == trackId);
      });
    }
  }

  @override
  void initState() {
    for (int i = 0; i < institutionTracks.length; i++) {
      print(institutionTracks[i].institutionTrackId);
    }
    print('length ${institutionTracks.length}');
    institutionTracks.forEach((e) {
      print(institutionTracks.indexOf(e));
      trackCardList.add(TrackCard(
        removeTrackCard,
        trackId: e.institutionTrackId,
        trackName: e.title,
      ));
    });
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<Institution>(context, listen: false)
            .fetchAndSetInstitutions());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final institution = Provider.of<Institution>(context);
    final institutionTraining = Provider.of<InstitutionTraining>(context);
    final auth = Provider.of<Auth>(context);

    return Container(
      child: Column(
        children: [
          Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'المركز',
                        style: TITLE_STYLE,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  DefaultInstitutionDropDown(
                    label: 'المركز الذي درست فيه',
                    onChanged: (Institution value) {
                      dropdownValueInstitutions = value.id;
                      institutionTraining.fetchAndSetTracksByInstitution(
                          dropdownValueInstitutions);
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'الرغبات',
                        style: SUBTITLE,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Consumer<InstitutionTraining>(
                    builder: (context, model, child) {
                      return DropdownSearch<InstitutionTraining>(
                          dropdownBuilderSupportsNullItem: true,
                          showSearchBox: true,
                          label: "الرغبات",
                          items: model.tracksOfInstitution,
                          // onFind: (String filter) =>
                          //     model.fetchAndSetTracksByInstitution(
                          //         dropdownValueInstitutions),
                          itemAsString: (InstitutionTraining u) =>
                              u.eductionTrack.arTitle,
                          onChanged: (InstitutionTraining data) {
                            setState(() {
                              trackCardList.add(TrackCard(
                                removeTrackCard,
                                trackId: data?.institutionId,
                                trackName: data.eductionTrack.arTitle,
                              ));
                              model.tracksOfInstitution.removeWhere(
                                  (element) => element.id == data.id);
                            });
                            finalTracks.add(TrackObject(
                                InstitutionTrackId: data.id,
                                order: finalTracks.length + 1));
                          });
                    },
                  ),
                  Column(
                    children: trackCardList,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTracksSearchDropdown(
      InstitutionTraining model, Institution institution) {
    return SearchableDropdown.single(
      searchFn: institution.searchfn,
      items: model.tracksOfInstitution.map((InstitutionTraining value) {
        // print(value.eductionTrack.arTitle);
        return DropdownMenuItem<InstitutionTraining>(
          value: value,
          child: Text(
            value?.eductionTrack?.arTitle,
          ),
        );
      })?.toList(),
      value: model.dropdownValueTrack,
      hint: "اختر الرغبة",
      searchHint: null,
      underline: Container(),
      onChanged: (value) {
        setState(() {
          model.dropdownValueTrack = value;
        });
      },
      isExpanded: true,
    );
  }
}

class TrackCard extends StatelessWidget {
  final String trackId;
  final Function(TrackCard) removeTrackCard;
  final String trackName;

  const TrackCard(this.removeTrackCard,
      {Key key, @required this.trackId, this.trackName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final institutionTraining = Provider.of<InstitutionTraining>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(trackName),
        IconButton(
          onPressed: () {
            removeTrackCard(this);
            institutionTraining.tracksOfInstitution.add(
              InstitutionTraining(
                id: trackId,
                eductionTrack: EductionTrack(arTitle: trackName),
              ),
            );
          },
          icon: Icon(Icons.delete),
        )
      ],
    );
  }
}
