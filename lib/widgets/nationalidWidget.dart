import 'package:flutter/material.dart';
import 'package:pvtd_app/common/styles.dart';

class NationalidWidget extends StatelessWidget {
  final String text;
  final String hinttext;
  final TextEditingController controller;

  const NationalidWidget({
    @required this.text,
    @required this.hinttext,
    @required this.controller,
  }) : super();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              text,
              style: SUBTITLE,
              textAlign: TextAlign.right,
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'من فضلك ادخل رقمك القومي';
            }
            if (value.length < 14) {
              return 'الرقم القومي مكون من ١٤ رقم';
            }
            return null;
          },
          keyboardType: TextInputType.number,
          textAlign: TextAlign.right,
          controller: controller,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
