import 'package:flutter/material.dart';

class PasswordWidget extends StatelessWidget {
  final String text;
  final String hinttext;
  final TextEditingController controller;

  const PasswordWidget({
    @required this.text,
    @required this.hinttext,
    @required this.controller,
  }) : super();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(text,
                style: const TextStyle(
                    color: const Color(0xff26262b),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Almarai",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0),
                textAlign: TextAlign.right),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'من فضلك ادخل كلمة المرور';
            }
            if (value.length < 8) {
              return 'كلمة المرور تتكون من ٨ عناصر على الاقل';
            }
            return null;
          },
          obscureText: true,
          controller: controller,
          textAlign: TextAlign.right,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
