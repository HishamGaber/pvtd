import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class EmailWidget extends StatelessWidget {
  final TextEditingController controller;
  final String text;
  final String hinttext;

  const EmailWidget(
      {@required this.controller, @required this.text, @required this.hinttext})
      : super();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(text,
                style: const TextStyle(
                    color: const Color(0xff26262b),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Almarai",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0),
                textAlign: TextAlign.right),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          validator: (value) {
            if (!(EmailValidator.validate(controller.text))) {
              return 'من فضلك ادخل بريد الكتروني صحيح';
            }
            return null;
          },
          keyboardType: TextInputType.emailAddress,
          controller: controller,
          textAlign: TextAlign.right,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
