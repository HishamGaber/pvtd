import 'package:flutter/material.dart';
import 'package:pvtd_app/common/styles.dart';

class TextWidget extends StatelessWidget {
  final String text;
  final String hinttext;
  final TextEditingController controller;

  // final TextEditingController usernameController = TextEditingController();

  TextWidget(
      {@required this.text, @required this.hinttext, @required this.controller})
      : super();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(text, style: SUBTITLE),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        TextFormField(
          controller: controller,
          textAlign: TextAlign.right,
          decoration: InputDecoration(
            hintText: hinttext,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'من فضلك ادخل المطلوب';
            }
            return null;
          },
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
